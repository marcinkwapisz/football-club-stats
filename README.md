## Football club statistics (back-end)
The project was made by a player of one of the teams participating in the amateur football league [WLPN](http://wiejskaliga.pl). It aims to collect the results of clubs in the league as well as to collect more accurate statistics.


**This project was written using:**
*Java Spring Boot, Maven, JPA, Hibernate, H2, PostreSQL, ModelMapper, JUnit, Mockito, RESP API*


##### Its functionalities: 
* handling of the league, club, match and player (CRUD operations)
* information about the match: location, date, score, goal scorers
* sorted player list and match statistics for the club
* goal statistics for a player
* schedule and sorted scoreboard for the league, which are automatically updated on the match change

List of all endpoints and examples of queries made available as [Postman's collection](https://www.getpostman.com/collections/59a392a064ad3ba4c74b).


##### Development opportunities and further plans: 
* visual part of the project (front-end)
* using Docker to containerize the project
* security, division into administrator and user rights