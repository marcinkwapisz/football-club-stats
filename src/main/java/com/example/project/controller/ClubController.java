package com.example.project.controller;

import com.example.project.dto.ClubDTO;
import com.example.project.model.Player;
import com.example.project.service.ClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/clubs")
public class ClubController {

    @Autowired
    private ClubService clubService;

    @GetMapping()
    public ResponseEntity<Object> getClubsByPhraseInName(Pageable pageable, @RequestParam("phrase") String phrase) {
        try {
            return new ResponseEntity<>(clubService.getClubsByPhraseInName(pageable, phrase), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getClubById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(clubService.getClubById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> addClub(@RequestBody ClubDTO clubDTO) {
        clubService.addClub(clubDTO);
        return new ResponseEntity<>("Club created successfully", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updateClub(@PathVariable("id") Long id, @RequestBody ClubDTO clubDTO) {
        try {
            clubService.updateClub(id, clubDTO);
            return new ResponseEntity<>("Club updated successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}/players")
    public ResponseEntity<Object> updateListOfPlayersInClub(@PathVariable("id") Long id, @RequestBody List<Player> players) {
        try {
            clubService.updateListOfPlayersInClub(id, players);
            return new ResponseEntity<>("List of players updated successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteClub(@PathVariable("id") Long id) {
        clubService.deleteClubById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
