package com.example.project.controller;

import com.example.project.dto.LeagueClubStatsDTO;
import com.example.project.dto.LeagueDTO;
import com.example.project.service.ClubStatsService;
import com.example.project.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/leagues")
public class LeagueController {
    @Autowired
    private LeagueService leagueService;
    @Autowired
    private ClubStatsService clubStatsService;

    @GetMapping()
    public ResponseEntity<Object> getLeaguesByPhraseInLocation(Pageable pageable, @RequestParam("phrase") String phrase) {
        try {
            return new ResponseEntity<>(leagueService.getLeaguesByPhraseInName(pageable, phrase), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getLeagueById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(leagueService.getLeagueById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}/matchSchedule")
    public ResponseEntity<Object> getMatchScheduleByLeagueId(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(leagueService.getMatchScheduleByLeagueId(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}/goalScorers")
    public ResponseEntity<Object> getGoalScorersByLeagueId(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(leagueService.getPlayerStatsByLeagueId(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> addLeague(@RequestBody LeagueDTO leagueDTO) {
        leagueService.addLeague(leagueDTO);
        return new ResponseEntity<>("League created successfully", HttpStatus.CREATED);
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<Object> addClubStatsToLeague(@PathVariable("id") Long id,
                                                       @RequestBody LeagueClubStatsDTO leagueClubStatsDTO) {
        try {
            clubStatsService.addClubStatsToLeague(id, leagueClubStatsDTO.getScoreboard());
            return new ResponseEntity<>("Club stats added to league successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updateLeague(@PathVariable("id") Long id, @RequestBody LeagueDTO leagueDTO) {
        try {
            leagueService.updateLeague(id, leagueDTO);
            return new ResponseEntity<>("League updated successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteLeague(@PathVariable("id") Long id) {
        leagueService.deleteLeagueById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
