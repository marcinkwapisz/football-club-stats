package com.example.project.controller;

import com.example.project.dto.MatchDTO;
import com.example.project.dto.MatchResultDTO;
import com.example.project.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.DateTimeException;

@RestController
@RequestMapping("/matches")
public class MatchController {

    @Autowired
    private MatchService matchService;

    @GetMapping()
    public ResponseEntity<Object> getMatchesByPhraseInLocation(Pageable pageable, @RequestParam("phrase") String phrase) {
        try {
            return new ResponseEntity<>(matchService.getMatchesByPhraseInLocation(pageable, phrase), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getMatchById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(matchService.getMatchById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> addMatch(@RequestBody MatchDTO matchDTO) {
        matchService.addMatch(matchDTO);
        return new ResponseEntity<>("Match created successfully", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updateMatch(@PathVariable("id") Long id, @RequestBody MatchDTO matchDTO) {
        try {
            matchService.updateMatch(id, matchDTO);
            return new ResponseEntity<>("Match updated successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}/goals")
    public ResponseEntity<Object> setGoalScorers(@PathVariable("id") Long id, @RequestBody MatchResultDTO matchResultDTO) {
        try {
            if (matchResultDTO.getHomeClubGoalScorers() == null && matchResultDTO.getAwayClubGoalScorers() == null) {
                matchService.setNumberOfGoals(id, matchResultDTO);
                return new ResponseEntity<>("Number of goals set successfully", HttpStatus.OK);
            } else {
                matchService.setGoalScorers(id, matchResultDTO);
                return new ResponseEntity<>("Goals scorers and number of goals set successfully", HttpStatus.OK);
            }
        } catch (DateTimeException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.METHOD_NOT_ALLOWED);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteMatch(@PathVariable("id") Long id) {
        matchService.deleteMatchById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
