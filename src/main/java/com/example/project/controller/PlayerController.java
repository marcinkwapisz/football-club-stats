package com.example.project.controller;

import com.example.project.model.Player;
import com.example.project.service.PlayerService;
import com.example.project.service.PlayerStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerStatsService playerStatsService;

    @GetMapping()
    public ResponseEntity<Object> getPlayersByPhraseInName(Pageable pageable, @RequestParam("phrase") String phrase) {
        try {
            return new ResponseEntity<>(playerService.getPlayersByPhraseInName(pageable, phrase), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getPlayerById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(playerService.getPlayerById(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}/playerStats")
    public ResponseEntity<Object> getPlayerStatsByPlayerId(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(playerStatsService.getSetOfPlayerStatsByPlayerId(id), HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> addPlayer(@RequestBody Player player) {
        playerService.addPlayer(player);
        return new ResponseEntity<>("Player created successfully", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updatePlayer(@PathVariable("id") Long id, @RequestBody Player player) {
        try {
            playerService.updatePlayer(id, player);
            return new ResponseEntity<>("Player updated successfully", HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deletePlayer(@PathVariable("id") Long id) {
        playerService.deletePlayerById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
