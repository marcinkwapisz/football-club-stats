package com.example.project.dto;

import com.example.project.model.ClubStats;

import java.util.Set;

public class LeagueClubStatsDTO {
    private Set<ClubStats> scoreboard;

    public Set<ClubStats> getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(Set<ClubStats> scoreboard) {
        this.scoreboard = scoreboard;
    }
}
