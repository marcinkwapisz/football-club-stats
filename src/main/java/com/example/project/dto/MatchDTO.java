package com.example.project.dto;

import com.example.project.model.Club;
import com.example.project.model.League;

import java.time.LocalDateTime;

public class MatchDTO {

    private String location;

    private LocalDateTime date;

    private Club homeClub;

    private Club awayClub;

    private League league;

    public String getLocation() {
        return location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Club getHomeClub() {
        return homeClub;
    }

    public void setHomeClub(Club homeClub) {
        this.homeClub = homeClub;
    }

    public Club getAwayClub() {
        return awayClub;
    }

    public void setAwayClub(Club awayClub) {
        this.awayClub = awayClub;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }
}
