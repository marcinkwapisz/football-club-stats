package com.example.project.dto;

import com.example.project.model.GoalScorer;

import java.util.List;

public class MatchResultDTO {

    private int homeClubGoals;

    private int awayClubGoals;

    private List<GoalScorer> homeClubGoalScorers;

    private List<GoalScorer> awayClubGoalScorers;

    public int getHomeClubGoals() {
        return homeClubGoals;
    }

    public void setHomeClubGoals(int homeClubGoals) {
        this.homeClubGoals = homeClubGoals;
    }

    public int getAwayClubGoals() {
        return awayClubGoals;
    }

    public void setAwayClubGoals(int awayClubGoals) {
        this.awayClubGoals = awayClubGoals;
    }

    public List<GoalScorer> getHomeClubGoalScorers() {
        return homeClubGoalScorers;
    }

    public void setHomeClubGoalScorers(List<GoalScorer> homeClubGoalScorers) {
        this.homeClubGoalScorers = homeClubGoalScorers;
    }

    public List<GoalScorer> getAwayClubGoalScorers() {
        return awayClubGoalScorers;
    }

    public void setAwayClubGoalScorers(List<GoalScorer> awayClubGoalScorers) {
        this.awayClubGoalScorers = awayClubGoalScorers;
    }
}
