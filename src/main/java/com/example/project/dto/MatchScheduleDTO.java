package com.example.project.dto;

import com.example.project.model.Club;
import com.example.project.model.League;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDateTime;

public class MatchScheduleDTO implements Comparable<MatchScheduleDTO> {
    private Long id;

    private String location;

    private LocalDateTime date;

    @JsonIgnoreProperties({"scoreboard", "goalScorers"})
    private League league;

    @JsonIgnoreProperties({"players"})
    private Club homeClub;

    @JsonIgnoreProperties({"players"})
    private Club awayClub;

    private int homeClubGoals;

    private int awayClubGoals;

    @Override
    public int compareTo(MatchScheduleDTO matchScheduleDTO) {
        return date.compareTo(matchScheduleDTO.getDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Club getHomeClub() {
        return homeClub;
    }

    public void setHomeClub(Club homeClub) {
        this.homeClub = homeClub;
    }

    public Club getAwayClub() {
        return awayClub;
    }

    public void setAwayClub(Club awayClub) {
        this.awayClub = awayClub;
    }

    public int getHomeClubGoals() {
        return homeClubGoals;
    }

    public void setHomeClubGoals(int homeClubGoals) {
        this.homeClubGoals = homeClubGoals;
    }

    public int getAwayClubGoals() {
        return awayClubGoals;
    }

    public void setAwayClubGoals(int awayClubGoals) {
        this.awayClubGoals = awayClubGoals;
    }
}
