package com.example.project.dto;

public interface PlayerStatsDTO {
    String getLeagueName();

    Integer getNumberOfAwayGoals();

    Integer getNumberOfHomeGoals();
}
