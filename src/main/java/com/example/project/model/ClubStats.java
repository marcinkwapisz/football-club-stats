package com.example.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Comparator;

@Entity
@Table(name = "club_stats")
public class ClubStats implements Comparable<ClubStats> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "club_id")
    @JsonIgnoreProperties({"players"})
    private Club club;

    @ManyToOne
    @JoinColumn(name = "league_id")
    @JsonIgnore
    private League league;

    private int numberOfMatches = 0;

    private int numberOfMatchesWon = 0;

    private int numberOfMatchesTied = 0;

    private int numberOfMatchesLost = 0;

    private int numberOfGoalsScored = 0;

    private int numberOfGoalsConceded = 0;

    private int goalDifference = 0;

    private int numberOfPoints = 0;

    public ClubStats() {
    }

    public ClubStats(Club club, League league) {
        this.club = club;
        this.league = league;
    }

    @Override
    public int compareTo(ClubStats clubStats) {
        return Comparator.comparing(ClubStats::getNumberOfPoints)
                .thenComparing(ClubStats::getGoalDifference)
                .thenComparing(ClubStats::getNumberOfGoalsScored)
                .thenComparing(ClubStats::getNumberOfMatchesWon)
                .thenComparing(ClubStats::getId)
                .compare(clubStats, this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public int getNumberOfMatches() {
        return numberOfMatches;
    }

    public void setNumberOfMatches(int numberOfMatches) {
        this.numberOfMatches = numberOfMatches;
    }

    public int getNumberOfMatchesWon() {
        return numberOfMatchesWon;
    }

    public void setNumberOfMatchesWon(int numberOfMatchesWon) {
        this.numberOfMatchesWon = numberOfMatchesWon;
    }

    public int getNumberOfMatchesTied() {
        return numberOfMatchesTied;
    }

    public void setNumberOfMatchesTied(int numberOfMatchesTied) {
        this.numberOfMatchesTied = numberOfMatchesTied;
    }

    public int getNumberOfMatchesLost() {
        return numberOfMatchesLost;
    }

    public void setNumberOfMatchesLost(int numberOfMatchesLost) {
        this.numberOfMatchesLost = numberOfMatchesLost;
    }

    public int getNumberOfGoalsScored() {
        return numberOfGoalsScored;
    }

    public void setNumberOfGoalsScored(int numberOfGoalsScored) {
        this.numberOfGoalsScored = numberOfGoalsScored;
    }

    public int getNumberOfGoalsConceded() {
        return numberOfGoalsConceded;
    }

    public void setNumberOfGoalsConceded(int numberOfGoalsConceded) {
        this.numberOfGoalsConceded = numberOfGoalsConceded;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }

    public int getNumberOfPoints() {
        return numberOfPoints;
    }

    public void setNumberOfPoints(int numberOfPoints) {
        this.numberOfPoints = numberOfPoints;
    }
}
