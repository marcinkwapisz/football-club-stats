package com.example.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@Table(name = "leagues")
public class League {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "league")
    @SortNatural
    private SortedSet<ClubStats> scoreboard = new TreeSet<>();

    @OneToMany(mappedBy = "league")
    @SortNatural
    @JsonIgnore
    private SortedSet<PlayerStats> goalScorers = new TreeSet<>();

    @OneToMany(mappedBy = "league")
    @JsonIgnore
    private Set<Match> matchSchedule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ClubStats> getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(SortedSet<ClubStats> scoreboard) {
        this.scoreboard = scoreboard;
    }

    public Set<PlayerStats> getGoalScorers() {
        return goalScorers;
    }

    public void setGoalScorers(SortedSet<PlayerStats> goalScorers) {
        this.goalScorers = goalScorers;
    }

    public Set<Match> getMatchSchedule() {
        return matchSchedule;
    }

    public void setMatchSchedule(Set<Match> matchSchedule) {
        this.matchSchedule = matchSchedule;
    }
}
