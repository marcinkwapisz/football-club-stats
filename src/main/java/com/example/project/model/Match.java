package com.example.project.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "matches")
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String location;

    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "league_id")
    @JsonIgnoreProperties({"scoreboard", "timetable", "matchSchedule", "goalScorers"})
    private League league;

    @ManyToOne
    @JoinColumn(name = "home_club_id")
    private Club homeClub;

    @ManyToOne
    @JoinColumn(name = "away_club_id")
    private Club awayClub;

    private int homeClubGoals;

    private int awayClubGoals;

    @OneToMany
    private List<GoalScorer> homeClubGoalScorers;

    @OneToMany
    private List<GoalScorer> awayClubGoalScorers;

    private boolean resultAddedToClubStats = false;

    private boolean goalScorersAddedToPlayerStats = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Club getHomeClub() {
        return homeClub;
    }

    public void setHomeClub(Club homeClub) {
        this.homeClub = homeClub;
    }

    public Club getAwayClub() {
        return awayClub;
    }

    public void setAwayClub(Club awayClub) {
        this.awayClub = awayClub;
    }

    public int getHomeClubGoals() {
        return homeClubGoals;
    }

    public void setHomeClubGoals(int homeClubGoals) {
        this.homeClubGoals = homeClubGoals;
    }

    public int getAwayClubGoals() {
        return awayClubGoals;
    }

    public void setAwayClubGoals(int awayClubGoals) {
        this.awayClubGoals = awayClubGoals;
    }

    public List<GoalScorer> getHomeClubGoalScorers() {
        return homeClubGoalScorers;
    }

    public void setHomeClubGoalScorers(List<GoalScorer> homeClubGoalScorers) {
        this.homeClubGoalScorers = homeClubGoalScorers;
    }

    public List<GoalScorer> getAwayClubGoalScorers() {
        return awayClubGoalScorers;
    }

    public void setAwayClubGoalScorers(List<GoalScorer> awayClubGoalScorers) {
        this.awayClubGoalScorers = awayClubGoalScorers;
    }

    public boolean isResultAddedToClubStats() {
        return resultAddedToClubStats;
    }

    public void setResultAddedToClubStats(boolean resultAddedToClubStats) {
        this.resultAddedToClubStats = resultAddedToClubStats;
    }

    public boolean isGoalScorersAddedToPlayerStats() {
        return goalScorersAddedToPlayerStats;
    }

    public void setGoalScorersAddedToPlayerStats(boolean goalScorersAddedToPlayerStats) {
        this.goalScorersAddedToPlayerStats = goalScorersAddedToPlayerStats;
    }
}
