package com.example.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Comparator;

@Entity
@Table(name = "player_stats")
public class PlayerStats implements Comparable<PlayerStats> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "league_id")
    @JsonIgnore
    private League league;

    private int numberOfAwayGoals = 0;

    private int numberOfHomeGoals = 0;

    public PlayerStats() {
    }

    public PlayerStats(Player player, League league) {
        this.player = player;
        this.league = league;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public int getNumberOfAwayGoals() {
        return numberOfAwayGoals;
    }

    public void setNumberOfAwayGoals(int numberOfAwayGoals) {
        this.numberOfAwayGoals = numberOfAwayGoals;
    }

    public int getNumberOfHomeGoals() {
        return numberOfHomeGoals;
    }

    public void setNumberOfHomeGoals(int numberOfHomeGoals) {
        this.numberOfHomeGoals = numberOfHomeGoals;
    }

    public int getSumOfGoals() {
        return numberOfAwayGoals + numberOfHomeGoals;
    }

    @Override
    public int compareTo(PlayerStats playerStats) {
        return Comparator.comparing(PlayerStats::getSumOfGoals)
                .thenComparing(PlayerStats::getNumberOfAwayGoals)
                .thenComparing(PlayerStats::getId)
                .compare(playerStats, this);
    }
}
