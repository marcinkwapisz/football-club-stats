package com.example.project.repository;

import com.example.project.model.Club;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {
    Optional<Club> findById(Long id);

    Optional<Page<Club>> findByNameContainingIgnoreCase(Pageable pageable, String phrase);
}
