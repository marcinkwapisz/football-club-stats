package com.example.project.repository;

import com.example.project.model.ClubStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClubStatsRepository extends JpaRepository<ClubStats, Long> {
    ClubStats findByLeagueIdAndClubId(Long leagueId, Long clubId);
}
