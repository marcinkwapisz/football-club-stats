package com.example.project.repository;

import com.example.project.model.GoalScorer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GoalScorerRepository extends JpaRepository<GoalScorer, Long> {
    Optional<GoalScorer> findById(Long id);
}
