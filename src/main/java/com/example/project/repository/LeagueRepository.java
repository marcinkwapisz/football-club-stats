package com.example.project.repository;

import com.example.project.model.League;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LeagueRepository extends JpaRepository<League, Long> {
    Optional<League> findById(Long id);

    Optional<Page<League>> findByNameContainingIgnoreCase(Pageable pageable, String phrase);
}
