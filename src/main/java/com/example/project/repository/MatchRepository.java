package com.example.project.repository;

import com.example.project.model.Match;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
    Optional<Match> findById(Long id);

    Optional<Page<Match>> findByLocationContainingIgnoreCase(Pageable pageable, String phrase);
}
