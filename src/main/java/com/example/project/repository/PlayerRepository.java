package com.example.project.repository;

import com.example.project.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    Optional<Player> findById(Long id);

    Optional<Page<Player>> findByNameContainingIgnoreCase(Pageable pageable, String phrase);
}
