package com.example.project.repository;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.PlayerStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PlayerStatsRepository extends JpaRepository<PlayerStats, Long> {
    PlayerStats findByLeagueIdAndPlayerId(Long leagueId, Long playerId);

    @Query(value = "SELECT l.name AS leagueName, p.number_of_away_goals AS numberOfAwayGoals, p.number_of_home_goals " +
            "AS numberOfHomeGoals FROM player_stats p, leagues l WHERE p.player_id = ?1 AND p.league_id = l.id",
            nativeQuery = true)
    Set<PlayerStatsDTO> findSetOfPlayerStatsByPlayerId(Long id);
}
