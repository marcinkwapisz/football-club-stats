package com.example.project.service;

import com.example.project.dto.ClubDTO;
import com.example.project.model.Club;
import com.example.project.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClubService {
    Page<ClubDTO> getClubsByPhraseInName(Pageable pageable, String phrase);

    Club getClubById(Long id);

    void addClub(ClubDTO clubDTO);

    void updateClub(Long idOfClubToUpdate, ClubDTO clubDTO);

    void updateListOfPlayersInClub(Long idOfClubToUpdate, List<Player> players);

    void deleteClubById(Long idOfClubToDelete);
}
