package com.example.project.service;

import com.example.project.model.ClubStats;
import com.example.project.model.Match;

import java.util.Set;

public interface ClubStatsService {
    void addClubStatsToLeague(Long idOfLeague, Set<ClubStats> clubStatsSet);

    void updateClubStatsAfterMatch(Match match);

    void clearClubStatsBeforeMatchUpdate(Match match);

    void deleteClubStats(Long idOfClubStatsToDelete);
}
