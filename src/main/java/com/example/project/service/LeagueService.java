package com.example.project.service;

import com.example.project.dto.LeagueDTO;
import com.example.project.dto.MatchScheduleDTO;
import com.example.project.model.League;
import com.example.project.model.Match;
import com.example.project.model.PlayerStats;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface LeagueService {
    Page<League> getLeaguesByPhraseInName(Pageable pageable, String phrase);

    League getLeagueById(Long id);

    Set<MatchScheduleDTO> getMatchScheduleByLeagueId(Long id);

    Set<PlayerStats> getPlayerStatsByLeagueId(Long id);

    void addLeague(LeagueDTO leagueDTO);

    void updateLeague(Long idOfLeagueToUpdate, LeagueDTO leagueDTO);

    void updateMatchSchedule(Match match);

    void deleteLeagueById(Long idOfLeagueToDelete);
}
