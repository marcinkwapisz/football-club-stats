package com.example.project.service;

import com.example.project.dto.MatchDTO;
import com.example.project.dto.MatchResultDTO;
import com.example.project.dto.MatchToReturnDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MatchService {
    Page<MatchToReturnDTO> getMatchesByPhraseInLocation(Pageable pageable, String phrase);

    MatchToReturnDTO getMatchById(Long id);

    void addMatch(MatchDTO matchDTO);

    void updateMatch(Long idOfMatchToUpdate, MatchDTO matchDTO);

    void setGoalScorers(Long idOfMatchToSetGoalScorers, MatchResultDTO matchResultDTO);

    void setNumberOfGoals(Long idOfMatchToUpdate, MatchResultDTO matchResultDTO);

    void deleteMatchById(Long idOfMatchToDelete);
}
