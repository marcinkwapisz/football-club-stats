package com.example.project.service;

import com.example.project.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PlayerService {
    Page<Player> getPlayersByPhraseInName(Pageable pageable, String phrase);

    Player getPlayerById(Long id);

    void addPlayer(Player playerToAdd);

    void updatePlayer(Long idOfPlayerToUpdate, Player updatedPlayer);

    void deletePlayerById(Long idOfPlayerToDelete);
}
