package com.example.project.service;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.Match;

import java.util.Set;

public interface PlayerStatsService {
    Set<PlayerStatsDTO> getSetOfPlayerStatsByPlayerId(Long id);

    void updatePlayerStatsAfterMatch(Match match);

    void clearPlayerStatsBeforeMatchUpdate(Match match);

    void deletePlayerStats(Long idOfPlayerStatsToDelete);
}
