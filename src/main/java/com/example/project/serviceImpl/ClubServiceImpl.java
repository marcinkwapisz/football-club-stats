package com.example.project.serviceImpl;

import com.example.project.dto.ClubDTO;
import com.example.project.model.Club;
import com.example.project.model.Player;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.PlayerRepository;
import com.example.project.service.ClubService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClubServiceImpl implements ClubService {
    private final ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private ClubRepository clubRepository;
    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public Page<ClubDTO> getClubsByPhraseInName(Pageable pageable, String phrase) {
        Page<Club> clubPage = clubRepository.findByNameContainingIgnoreCase(pageable, phrase)
                .orElseThrow(() -> new EntityNotFoundException("No clubs were found associating with this phrase"));
        List<ClubDTO> clubDTOS = clubPage.stream().map(club -> modelMapper.map(club, ClubDTO.class))
                .collect(Collectors.toList());
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > clubDTOS.size() ? clubDTOS.size() : (start + pageable.getPageSize());
        return new PageImpl<>(clubDTOS.subList((int) start, (int) end), pageable, clubDTOS.size());
    }

    @Override
    public Club getClubById(Long id) {
        return clubRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Club with given id does not exist"));
    }

    @Override
    public void addClub(ClubDTO clubDTO) {
        clubRepository.save(modelMapper.map(clubDTO, Club.class));
    }

    @Override
    public void updateClub(Long idOfClubToUpdate, ClubDTO clubDTO) {
        clubRepository.findById(idOfClubToUpdate).map(club -> {
            club.setName(clubDTO.getName());
            return clubRepository.save(club);
        }).orElseThrow(() -> new EntityNotFoundException("Club with given id does not exist"));
    }

    @Override
    public void updateListOfPlayersInClub(Long idOfClubToUpdate, List<Player> players) {
        clubRepository.findById(idOfClubToUpdate).map(club -> {
            if (players != null) {
                for (Player player : players) {
                    playerRepository.findById(player.getId()).ifPresent(foundPlayer -> {
                        foundPlayer.setClub(club);
                        playerRepository.save(foundPlayer);
                    });
                }
            }
            return clubRepository.save(club);
        }).orElseThrow(() -> new EntityNotFoundException("Club with given id does not exist"));
    }

    @Override
    public void deleteClubById(Long idOfClubToDelete) {
        clubRepository.findById(idOfClubToDelete).ifPresent(club -> {
            if (club.getPlayers() != null) {
                List<Player> players = club.getPlayers();
                for (Player player : players) {
                    player.setClub(null);
                    playerRepository.save(player);
                }
            }
            clubRepository.delete(club);
        });
    }
}
