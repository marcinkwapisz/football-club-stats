package com.example.project.serviceImpl;

import com.example.project.model.Club;
import com.example.project.model.ClubStats;
import com.example.project.model.Match;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.ClubStatsRepository;
import com.example.project.repository.LeagueRepository;
import com.example.project.service.ClubStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ClubStatsServiceImpl implements ClubStatsService {
    @Autowired
    private ClubStatsRepository clubStatsRepository;

    @Autowired
    private LeagueRepository leagueRepository;

    @Autowired
    private ClubRepository clubRepository;

    @Override
    public void addClubStatsToLeague(Long idOfLeague, Set<ClubStats> clubStatsSet) {
        Set<Long> clubsIds = clubStatsSet.stream().map(ClubStats::getClub).collect(Collectors.toSet())
                .stream().map(Club::getId).collect(Collectors.toSet());
        leagueRepository.findById(idOfLeague).map(league -> {
            for (Long clubId : clubsIds) {
                ClubStats clubStats = new ClubStats(clubRepository.getOne(clubId), league);
                league.getScoreboard().add(clubStatsRepository.save(clubStats));
            }
            return leagueRepository.save(league);
        }).orElseThrow(() -> new EntityNotFoundException("League with given id does not exist"));
    }

    @Override
    public void updateClubStatsAfterMatch(Match match) {
        addMatchResultToClubStats(clubStatsRepository.findByLeagueIdAndClubId(match.getLeague().getId(),
                match.getHomeClub().getId()), match.getHomeClubGoals(), match.getAwayClubGoals());
        addMatchResultToClubStats(clubStatsRepository.findByLeagueIdAndClubId(match.getLeague().getId(),
                match.getAwayClub().getId()), match.getAwayClubGoals(), match.getHomeClubGoals());
    }

    private void addMatchResultToClubStats(ClubStats clubStats, int homeClubGoals, int awayClubGoals) {
        clubStats.setNumberOfMatches(clubStats.getNumberOfMatches() + 1);
        if (homeClubGoals > awayClubGoals) {
            clubStats.setNumberOfMatchesWon(clubStats.getNumberOfMatchesWon() + 1);
            clubStats.setNumberOfPoints(clubStats.getNumberOfPoints() + 3);
        } else if (homeClubGoals == awayClubGoals) {
            clubStats.setNumberOfMatchesTied(clubStats.getNumberOfMatchesTied() + 1);
            clubStats.setNumberOfPoints(clubStats.getNumberOfPoints() + 1);
        } else {
            clubStats.setNumberOfMatchesLost(clubStats.getNumberOfMatchesLost() + 1);
        }
        clubStats.setNumberOfGoalsScored(clubStats.getNumberOfGoalsScored() + homeClubGoals);
        clubStats.setNumberOfGoalsConceded(clubStats.getNumberOfGoalsConceded() + awayClubGoals);
        clubStats.setGoalDifference(clubStats.getNumberOfGoalsScored() - clubStats.getNumberOfGoalsConceded());
        clubStatsRepository.save(clubStats);
    }

    @Override
    public void clearClubStatsBeforeMatchUpdate(Match match) {
        removeMatchResultFromClubStats(clubStatsRepository.findByLeagueIdAndClubId(match.getLeague().getId(),
                match.getHomeClub().getId()), match.getHomeClubGoals(), match.getAwayClubGoals());
        removeMatchResultFromClubStats(clubStatsRepository.findByLeagueIdAndClubId(match.getLeague().getId(),
                match.getAwayClub().getId()), match.getAwayClubGoals(), match.getHomeClubGoals());
    }

    private void removeMatchResultFromClubStats(ClubStats clubStats, int homeClubGoals, int awayClubGoals) {
        clubStats.setNumberOfMatches(clubStats.getNumberOfMatches() - 1);
        if (homeClubGoals > awayClubGoals) {
            clubStats.setNumberOfMatchesWon(clubStats.getNumberOfMatchesWon() - 1);
            clubStats.setNumberOfPoints(clubStats.getNumberOfPoints() - 3);
        } else if (homeClubGoals == awayClubGoals) {
            clubStats.setNumberOfMatchesTied(clubStats.getNumberOfMatchesTied() - 1);
            clubStats.setNumberOfPoints(clubStats.getNumberOfPoints() - 1);
        } else {
            clubStats.setNumberOfMatchesLost(clubStats.getNumberOfMatchesLost() - 1);
        }
        clubStats.setNumberOfGoalsScored(clubStats.getNumberOfGoalsScored() - homeClubGoals);
        clubStats.setNumberOfGoalsConceded(clubStats.getNumberOfGoalsConceded() - awayClubGoals);
        clubStats.setGoalDifference(clubStats.getNumberOfGoalsScored() - clubStats.getNumberOfGoalsConceded());
        clubStatsRepository.save(clubStats);
    }

    @Override
    public void deleteClubStats(Long idOfClubStatsToDelete) {
        clubStatsRepository.findById(idOfClubStatsToDelete).ifPresent(clubStatsRepository::delete);
    }
}
