package com.example.project.serviceImpl;

import com.example.project.dto.LeagueDTO;
import com.example.project.dto.MatchScheduleDTO;
import com.example.project.model.League;
import com.example.project.model.Match;
import com.example.project.model.PlayerStats;
import com.example.project.repository.LeagueRepository;
import com.example.project.service.LeagueService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service
public class LeagueServiceImpl implements LeagueService {
    private final ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private LeagueRepository leagueRepository;

    @Override
    public Page<League> getLeaguesByPhraseInName(Pageable pageable, String phrase) {
        return leagueRepository.findByNameContainingIgnoreCase(pageable, phrase)
                .orElseThrow(() -> new EntityNotFoundException("No leagues were found associating with this phrase"));
    }

    @Override
    public League getLeagueById(Long id) {
        return leagueRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("League with given id does not exist"));
    }

    @Override
    public TreeSet<MatchScheduleDTO> getMatchScheduleByLeagueId(Long id) {
        Set<Match> matches = getLeagueById(id).getMatchSchedule();
        return matches.stream().map(match -> modelMapper.map(match, MatchScheduleDTO.class))
                .sorted().collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public Set<PlayerStats> getPlayerStatsByLeagueId(Long id) {
        return getLeagueById(id).getGoalScorers().stream().sorted().collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public void addLeague(LeagueDTO leagueDTO) {
        leagueRepository.save(modelMapper.map(leagueDTO, League.class));
    }

    @Override
    public void updateLeague(Long idOfLeagueToUpdate, LeagueDTO leagueDTO) {
        leagueRepository.findById(idOfLeagueToUpdate).map(league -> {
            modelMapper.map(leagueDTO, league);
            return leagueRepository.save(league);
        }).orElseThrow(() -> new EntityNotFoundException("League with given id does not exist"));
    }

    @Override
    public void updateMatchSchedule(Match match) {
        leagueRepository.findById(match.getLeague().getId()).map(league -> {
            league.getMatchSchedule().add(match);
            return leagueRepository.save(league);
        }).orElseThrow(() -> new EntityNotFoundException("League with given id does not exist"));
    }

    @Override
    public void deleteLeagueById(Long idOfLeagueToDelete) {
        leagueRepository.findById(idOfLeagueToDelete).ifPresent(leagueRepository::delete);
    }
}
