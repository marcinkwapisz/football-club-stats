package com.example.project.serviceImpl;

import com.example.project.dto.MatchDTO;
import com.example.project.dto.MatchResultDTO;
import com.example.project.dto.MatchToReturnDTO;
import com.example.project.model.GoalScorer;
import com.example.project.model.Match;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.GoalScorerRepository;
import com.example.project.repository.MatchRepository;
import com.example.project.repository.PlayerRepository;
import com.example.project.service.ClubStatsService;
import com.example.project.service.LeagueService;
import com.example.project.service.MatchService;
import com.example.project.service.PlayerStatsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MatchServiceImpl implements MatchService {
    private final ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private ClubRepository clubRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private GoalScorerRepository goalScorerRepository;
    @Autowired
    private ClubStatsService clubStatsService;
    @Autowired
    private LeagueService leagueService;
    @Autowired
    private PlayerStatsService playerStatsService;

    @Override
    public Page<MatchToReturnDTO> getMatchesByPhraseInLocation(Pageable pageable, String phrase) {
        Page<Match> matchPage = matchRepository.findByLocationContainingIgnoreCase(pageable, phrase)
                .orElseThrow(() -> new EntityNotFoundException("No matches were found associating with this phrase"));
        List<MatchToReturnDTO> matchToReturnDTOS = matchPage.stream().map(match ->
                modelMapper.map(match, MatchToReturnDTO.class)).collect(Collectors.toList());
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > matchToReturnDTOS.size() ?
                matchToReturnDTOS.size() : (start + pageable.getPageSize());
        return new PageImpl<>(matchToReturnDTOS.subList((int) start, (int) end), pageable, matchToReturnDTOS.size());
    }

    @Override
    public MatchToReturnDTO getMatchById(Long id) {
        Match match = matchRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Match with given id does not exist"));
        return modelMapper.map(match, MatchToReturnDTO.class);
    }

    @Override
    public void addMatch(MatchDTO matchDTO) {
        if (matchDTO.getHomeClub() != null) {
            Long homeClubId = matchDTO.getHomeClub().getId();
            clubRepository.findById(homeClubId).ifPresent(matchDTO::setHomeClub);
        }
        if (matchDTO.getAwayClub() != null) {
            Long awayClubId = matchDTO.getAwayClub().getId();
            clubRepository.findById(awayClubId).ifPresent(matchDTO::setAwayClub);
        }
        if (matchDTO.getLeague() != null) {
            Long leagueId = matchDTO.getLeague().getId();
            matchDTO.setLeague(leagueService.getLeagueById(leagueId));
        }
        Match match = matchRepository.save(modelMapper.map(matchDTO, Match.class));
        leagueService.updateMatchSchedule(match);
    }

    @Override
    public void updateMatch(Long idOfMatchToUpdate, MatchDTO matchDTO) {
        matchRepository.findById(idOfMatchToUpdate).map(match -> {
            modelMapper.map(matchDTO, match);
            return matchRepository.save(match);
        }).orElseThrow(() -> new EntityNotFoundException("Match with given id does not exist"));
    }

    @Override
    public void setGoalScorers(Long idOfMatchToSetGoalScorers, MatchResultDTO matchResultDTO) {
        matchRepository.findById(idOfMatchToSetGoalScorers).map(match -> {
            if (match.getDate().isAfter(LocalDateTime.now()))
                throw new DateTimeException("You can not set the result for a future match");
            if (match.isResultAddedToClubStats())
                clubStatsService.clearClubStatsBeforeMatchUpdate(match);
            if (match.isGoalScorersAddedToPlayerStats())
                playerStatsService.clearPlayerStatsBeforeMatchUpdate(match);
            updateListOfGoalScorers(matchResultDTO.getHomeClubGoalScorers(), match.getHomeClubGoalScorers());
            updateListOfGoalScorers(matchResultDTO.getAwayClubGoalScorers(), match.getAwayClubGoalScorers());

            match.setHomeClubGoals(match.getHomeClubGoalScorers().stream().mapToInt(GoalScorer::getNumberOfGoals).sum());
            match.setAwayClubGoals(match.getAwayClubGoalScorers().stream().mapToInt(GoalScorer::getNumberOfGoals).sum());
            clubStatsService.updateClubStatsAfterMatch(match);
            match.setResultAddedToClubStats(true);
            playerStatsService.updatePlayerStatsAfterMatch(match);
            match.setGoalScorersAddedToPlayerStats(true);
            return matchRepository.save(match);
        }).orElseThrow(() -> new EntityNotFoundException("Match with given id does not exist"));
    }

    private void updateListOfGoalScorers(List<GoalScorer> goalScorersFromDTO, List<GoalScorer> goalScorers) {
        if (goalScorers.isEmpty()) {
            for (GoalScorer goalScorer : goalScorersFromDTO) {
                addGoalScorerToMatch(goalScorer, goalScorers);
            }
        } else {
            List<GoalScorer> goalScorersBeforeUpdate = new ArrayList<>(goalScorers);
            List<GoalScorer> goalScorersBeforeUpdateToBeRetained = new ArrayList<>();
            for (GoalScorer goalScorer : goalScorersFromDTO) {
                Optional<GoalScorer> foundGoalScorer = goalScorersBeforeUpdate.stream()
                        .filter(goalScorer1 -> goalScorer.getPlayer().getId().equals(goalScorer1.getPlayer().getId())
                                && goalScorer.getNumberOfGoals() == goalScorer1.getNumberOfGoals()).findAny();
                if (foundGoalScorer.isPresent()) {
                    goalScorersBeforeUpdateToBeRetained.add(foundGoalScorer.get());
                } else {
                    addGoalScorerToMatch(goalScorer, goalScorers);
                }
            }
            goalScorersBeforeUpdate.removeAll(goalScorersBeforeUpdateToBeRetained);
            for (GoalScorer goalScorer : goalScorersBeforeUpdate) {
                goalScorers.remove(goalScorer);
                goalScorerRepository.delete(goalScorer);
            }
        }
    }

    private void addGoalScorerToMatch(GoalScorer goalScorer, List<GoalScorer> goalScorers) {
        playerRepository.findById(goalScorer.getPlayer().getId()).ifPresent(goalScorer::setPlayer);
        goalScorers.add(goalScorerRepository.save(goalScorer));
    }

    @Override
    public void setNumberOfGoals(Long idOfMatchToUpdate, MatchResultDTO matchResultDTO) {
        matchRepository.findById(idOfMatchToUpdate).map(match -> {
            if (match.getDate().isAfter(LocalDateTime.now()))
                throw new DateTimeException("You can not set the result for a future match");
            if (match.isResultAddedToClubStats())
                clubStatsService.clearClubStatsBeforeMatchUpdate(match);
            match.setHomeClubGoals(matchResultDTO.getHomeClubGoals());
            match.setAwayClubGoals(matchResultDTO.getAwayClubGoals());
            clubStatsService.updateClubStatsAfterMatch(match);
            match.setResultAddedToClubStats(true);
            return matchRepository.save(match);
        }).orElseThrow(() -> new EntityNotFoundException("Match with given id does not exist"));
    }

    @Override
    public void deleteMatchById(Long idOfMatchToDelete) {
        matchRepository.findById(idOfMatchToDelete).ifPresent(match -> {
            if (match.isResultAddedToClubStats())
                clubStatsService.clearClubStatsBeforeMatchUpdate(match);
            if (match.isGoalScorersAddedToPlayerStats())
                playerStatsService.clearPlayerStatsBeforeMatchUpdate(match);
            matchRepository.delete(match);
        });
    }
}
