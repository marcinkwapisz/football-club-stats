package com.example.project.serviceImpl;

import com.example.project.model.Player;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.PlayerRepository;
import com.example.project.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ClubRepository clubRepository;

    @Override
    public Page<Player> getPlayersByPhraseInName(Pageable pageable, String phrase) {
        return playerRepository.findByNameContainingIgnoreCase(pageable, phrase)
                .orElseThrow(() -> new EntityNotFoundException("No players were found associating with this phrase"));
    }

    @Override
    public Player getPlayerById(Long id) {
        return playerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Player with given id does not exist"));
    }

    @Override
    public void addPlayer(Player playerToAdd) {
        if (playerToAdd.getClub() != null) {
            Long clubIdOfPlayerToAdd = playerToAdd.getClub().getId();
            clubRepository.findById(clubIdOfPlayerToAdd).ifPresent(playerToAdd::setClub);
        }
        playerRepository.save(playerToAdd);
    }

    @Override
    public void updatePlayer(Long idOfPlayerToUpdate, Player updatedPlayer) {
        playerRepository.findById(idOfPlayerToUpdate).map(player -> {
            player.setName(updatedPlayer.getName());
            if (updatedPlayer.getClub() != null) {
                Long clubIdOfPlayerToUpdate = updatedPlayer.getClub().getId();
                clubRepository.findById(clubIdOfPlayerToUpdate).ifPresent(player::setClub);
            }
            return playerRepository.save(player);
        }).orElseThrow(() -> new EntityNotFoundException("Player with given id does not exist"));
    }

    @Override
    public void deletePlayerById(Long idOfPlayerToDelete) {
        playerRepository.findById(idOfPlayerToDelete).ifPresent(playerRepository::delete);
    }
}
