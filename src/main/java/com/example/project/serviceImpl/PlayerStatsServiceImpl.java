package com.example.project.serviceImpl;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.GoalScorer;
import com.example.project.model.League;
import com.example.project.model.Match;
import com.example.project.model.PlayerStats;
import com.example.project.repository.PlayerStatsRepository;
import com.example.project.service.PlayerStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class PlayerStatsServiceImpl implements PlayerStatsService {
    @Autowired
    private PlayerStatsRepository playerStatsRepository;

    @Override
    public Set<PlayerStatsDTO> getSetOfPlayerStatsByPlayerId(Long id) {
        return playerStatsRepository.findSetOfPlayerStatsByPlayerId(id);
    }

    @Override
    public void updatePlayerStatsAfterMatch(Match match) {
        updatePlayerStatsFromGoalScorerList(match.getAwayClubGoalScorers(), match.getLeague(), true);
        updatePlayerStatsFromGoalScorerList(match.getHomeClubGoalScorers(), match.getLeague(), false);
    }

    private void updatePlayerStatsFromGoalScorerList(List<GoalScorer> goalScorerList, League league, boolean awayGoalScorers) {
        for (GoalScorer goalScorer : goalScorerList) {
            PlayerStats playerStats = playerStatsRepository.findByLeagueIdAndPlayerId(league.getId(),
                    goalScorer.getPlayer().getId());
            if (playerStats == null)
                playerStats = new PlayerStats(goalScorer.getPlayer(), league);
            if (awayGoalScorers)
                playerStats.setNumberOfAwayGoals(playerStats.getNumberOfAwayGoals() + goalScorer.getNumberOfGoals());
            else
                playerStats.setNumberOfHomeGoals(playerStats.getNumberOfHomeGoals() + goalScorer.getNumberOfGoals());
            league.getGoalScorers().add(playerStatsRepository.save(playerStats));
        }
    }

    @Override
    public void clearPlayerStatsBeforeMatchUpdate(Match match) {
        removeGoalScorersFromPlayerStats(match.getAwayClubGoalScorers(), match.getLeague(), true);
        removeGoalScorersFromPlayerStats(match.getHomeClubGoalScorers(), match.getLeague(), false);
    }

    private void removeGoalScorersFromPlayerStats(List<GoalScorer> goalScorerList, League league, boolean awayGoalScorers) {
        for (GoalScorer goalScorer : goalScorerList) {
            PlayerStats playerStats = playerStatsRepository.findByLeagueIdAndPlayerId(league.getId(),
                    goalScorer.getPlayer().getId());
            if (awayGoalScorers)
                playerStats.setNumberOfAwayGoals(playerStats.getNumberOfAwayGoals() - goalScorer.getNumberOfGoals());
            else
                playerStats.setNumberOfHomeGoals(playerStats.getNumberOfHomeGoals() - goalScorer.getNumberOfGoals());
            playerStatsRepository.save(playerStats);
            if (playerStats.getSumOfGoals() == 0) {
                league.getGoalScorers().remove(playerStats);
                deletePlayerStats(playerStats.getId());
            }
        }
    }

    @Override
    public void deletePlayerStats(Long idOfPlayerStatsToDelete) {
        playerStatsRepository.findById(idOfPlayerStatsToDelete).ifPresent(playerStatsRepository::delete);
    }
}
