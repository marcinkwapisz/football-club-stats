package com.example.project.controller;

import com.example.project.dto.ClubDTO;
import com.example.project.model.Club;
import com.example.project.model.Player;
import com.example.project.service.ClubService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ClubController.class)
public class ClubControllerTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ClubService clubService;
    private Club club;
    private ClubDTO clubDTO;
    private Page<ClubDTO> clubDTOPage;
    private Player player;

    @Before
    public void init() {
        club = new Club();
        club.setId(1L);
        club.setName("One");
        player = new Player();
        player.setId(1L);
        player.setName("Player");
        club.setPlayers(new ArrayList<>(Collections.singletonList(player)));
        clubDTO = new ClubDTO();
        clubDTO.setId(2L);
        clubDTO.setName("Two");
        List<ClubDTO> clubDTOList = new ArrayList<>(Collections.singletonList(clubDTO));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > clubDTOList.size() ? clubDTOList.size() : (start + pageable.getPageSize());
        clubDTOPage = new PageImpl<>(clubDTOList.subList((int) start, (int) end), pageable, clubDTOList.size());
    }

    @Test
    public void whenGetAll_thenShouldReturnPageWithClubDTOsByPhraseInName() throws Exception {
        when(clubService.getClubsByPhraseInName(any(Pageable.class), any(String.class))).thenReturn(clubDTOPage);
        mockMvc.perform(get("/clubs")
                .param("phrase", "tw")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(clubDTO.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(clubDTO.getName())));
    }

    @Test
    public void whenGetAll_thenShouldReturnNotFound() throws Exception {
        when(clubService.getClubsByPhraseInName(any(Pageable.class), any(String.class)))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/clubs")
                .param("phrase", "tw")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetById_thenShouldReturnClub() throws Exception {
        when(clubService.getClubById(anyLong())).thenReturn(club);
        mockMvc.perform(get("/clubs/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(club.getId().intValue())))
                .andExpect(jsonPath("$.name", is(club.getName())))
                .andExpect(jsonPath("$.players[0].id", is(player.getId().intValue())))
                .andExpect(jsonPath("$.players[0].name", is(player.getName())));
    }

    @Test
    public void whenGetById_thenShouldReturnNotFound() throws Exception {
        when(clubService.getClubById(anyLong())).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/clubs/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenAddClub_thenShouldReturnCreated() throws Exception {
        doNothing().when(clubService).addClub(any(ClubDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/clubs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clubDTO)))
                .andExpect(jsonPath("$", is("Club created successfully")))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenUpdateClub_thenShouldReturnOk() throws Exception {
        doNothing().when(clubService).updateClub(anyLong(), any(ClubDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/clubs/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clubDTO)))
                .andExpect(jsonPath("$", is("Club updated successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdateClub_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class).when(clubService).updateClub(anyLong(), any(ClubDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/clubs/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clubDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenUpdateListOfPlayersInClub_thenShouldReturnOk() throws Exception {
        doNothing().when(clubService).updateListOfPlayersInClub(anyLong(), anyList());
        mockMvc.perform(
                MockMvcRequestBuilders.put("/clubs/1/players")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(Collections.singleton(player))))
                .andExpect(jsonPath("$", is("List of players updated successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdateListOfPlayersInClub_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class).when(clubService).updateListOfPlayersInClub(anyLong(), anyList());
        mockMvc.perform(
                MockMvcRequestBuilders.put("/clubs/1/players")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(Collections.singleton(player))))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenDeleteClub_thenShouldReturnOk() throws Exception {
        doNothing().when(clubService).deleteClubById(anyLong());
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/clubs/1"))
                .andExpect(status().isOk());
    }
}
