package com.example.project.controller;

import com.example.project.dto.LeagueClubStatsDTO;
import com.example.project.dto.LeagueDTO;
import com.example.project.dto.MatchScheduleDTO;
import com.example.project.model.ClubStats;
import com.example.project.model.League;
import com.example.project.model.PlayerStats;
import com.example.project.service.ClubStatsService;
import com.example.project.service.LeagueService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LeagueController.class)
public class LeagueControllerTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private LeagueService leagueService;
    @MockBean
    private ClubStatsService clubStatsService;
    private League league;
    private Page<League> leaguePage;
    private ClubStats clubStats;
    private MatchScheduleDTO matchScheduleDTO;
    private PlayerStats playerStats;
    private LeagueDTO leagueDTO;
    private LeagueClubStatsDTO leagueClubStatsDTO;

    @Before
    public void init() {
        league = new League();
        league.setId(1L);
        league.setName("league");
        clubStats = new ClubStats();
        clubStats.setId(1L);
        league.setScoreboard(new TreeSet<>(Collections.singletonList(clubStats)));
        List<League> leagueList = new ArrayList<>(Collections.singletonList(league));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > leagueList.size() ? leagueList.size() : (start + pageable.getPageSize());
        leaguePage = new PageImpl<>(leagueList.subList((int) start, (int) end), pageable, leagueList.size());
        leagueDTO = new LeagueDTO();
        leagueDTO.setName("leagueDTO");
        matchScheduleDTO = new MatchScheduleDTO();
        matchScheduleDTO.setId(1L);
        playerStats = new PlayerStats();
        playerStats.setId(1L);
        leagueClubStatsDTO = new LeagueClubStatsDTO();
        leagueClubStatsDTO.setScoreboard(new TreeSet<>(Collections.singletonList(clubStats)));
    }

    @Test
    public void whenGetAll_thenShouldReturnPageWithLeaguesByPhraseInName() throws Exception {
        when(leagueService.getLeaguesByPhraseInName(any(Pageable.class), any(String.class)))
                .thenReturn(leaguePage);
        mockMvc.perform(get("/leagues")
                .param("phrase", "lea")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(league.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(league.getName())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].id", is(clubStats.getId().intValue())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].club", is(clubStats.getClub())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfMatches",
                        is(clubStats.getNumberOfMatches())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfMatchesWon",
                        is(clubStats.getNumberOfMatchesWon())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfMatchesTied",
                        is(clubStats.getNumberOfMatchesTied())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfMatchesLost",
                        is(clubStats.getNumberOfMatchesLost())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfGoalsScored",
                        is(clubStats.getNumberOfGoalsScored())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfGoalsConceded",
                        is(clubStats.getNumberOfGoalsConceded())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].goalDifference",
                        is(clubStats.getGoalDifference())))
                .andExpect(jsonPath("$.content[0].scoreboard[0].numberOfPoints",
                        is(clubStats.getNumberOfPoints())));
    }

    @Test
    public void whenGetAll_thenShouldReturnNotFound() throws Exception {
        when(leagueService.getLeaguesByPhraseInName(any(Pageable.class), any(String.class)))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/leagues")
                .param("phrase", "lea")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetById_thenShouldReturnLeague() throws Exception {
        when(leagueService.getLeagueById(anyLong()))
                .thenReturn(league);
        mockMvc.perform(get("/leagues/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(league.getId().intValue())))
                .andExpect(jsonPath("$.name", is(league.getName())))
                .andExpect(jsonPath("$.scoreboard[0].id", is(clubStats.getId().intValue())))
                .andExpect(jsonPath("$.scoreboard[0].club", is(clubStats.getClub())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfMatches",
                        is(clubStats.getNumberOfMatches())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfMatchesWon",
                        is(clubStats.getNumberOfMatchesWon())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfMatchesTied",
                        is(clubStats.getNumberOfMatchesTied())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfMatchesLost",
                        is(clubStats.getNumberOfMatchesLost())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfGoalsScored",
                        is(clubStats.getNumberOfGoalsScored())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfGoalsConceded",
                        is(clubStats.getNumberOfGoalsConceded())))
                .andExpect(jsonPath("$.scoreboard[0].goalDifference",
                        is(clubStats.getGoalDifference())))
                .andExpect(jsonPath("$.scoreboard[0].numberOfPoints",
                        is(clubStats.getNumberOfPoints())));
    }

    @Test
    public void whenGetById_thenShouldReturnNotFound() throws Exception {
        when(leagueService.getLeagueById(anyLong()))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/leagues/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetMatchScheduleByLeagueId_thenShouldReturnMatchScheduleDTOSet() throws Exception {
        when(leagueService.getMatchScheduleByLeagueId(anyLong()))
                .thenReturn(new HashSet<>(Collections.singletonList(matchScheduleDTO)));
        mockMvc.perform(get("/leagues/1/matchSchedule")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(matchScheduleDTO.getId().intValue())))
                .andExpect(jsonPath("$[0].location", is(matchScheduleDTO.getLocation())))
                .andExpect(jsonPath("$[0].date", is(matchScheduleDTO.getDate())))
                .andExpect(jsonPath("$[0].league", is(matchScheduleDTO.getLeague())))
                .andExpect(jsonPath("$[0].homeClub", is(matchScheduleDTO.getHomeClub())))
                .andExpect(jsonPath("$[0].awayClub", is(matchScheduleDTO.getAwayClub())))
                .andExpect(jsonPath("$[0].homeClubGoals", is(matchScheduleDTO.getHomeClubGoals())))
                .andExpect(jsonPath("$[0].awayClubGoals", is(matchScheduleDTO.getAwayClubGoals())));
    }

    @Test
    public void whenGetMatchScheduleByLeagueId_thenShouldReturnNotFound() throws Exception {
        when(leagueService.getMatchScheduleByLeagueId(anyLong()))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/leagues/1/matchSchedule")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetGoalScorersByLeagueId_thenShouldReturnGoalScorersSet() throws Exception {
        when(leagueService.getPlayerStatsByLeagueId(anyLong()))
                .thenReturn(new TreeSet<>(Collections.singletonList(playerStats)));
        mockMvc.perform(get("/leagues/1/goalScorers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(playerStats.getId().intValue())))
                .andExpect(jsonPath("$[0].player", is(playerStats.getPlayer())))
                .andExpect(jsonPath("$[0].numberOfHomeGoals", is(playerStats.getNumberOfHomeGoals())))
                .andExpect(jsonPath("$[0].numberOfAwayGoals", is(playerStats.getNumberOfAwayGoals())))
                .andExpect(jsonPath("$[0].sumOfGoals", is(playerStats.getSumOfGoals())));
    }

    @Test
    public void whenGetGoalScorersByLeagueId_thenShouldReturnNotFound() throws Exception {
        when(leagueService.getPlayerStatsByLeagueId(anyLong()))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/leagues/1/goalScorers")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenAddClub_thenShouldReturnCreated() throws Exception {
        doNothing()
                .when(leagueService).addLeague(any(LeagueDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/leagues")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(leagueDTO)))
                .andExpect(jsonPath("$", is("League created successfully")))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenAddClubStatsToLeague_thenShouldReturnOk() throws Exception {
        doNothing()
                .when(clubStatsService).addClubStatsToLeague(anyLong(), anySet());
        mockMvc.perform(
                MockMvcRequestBuilders.post("/leagues/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(leagueClubStatsDTO)))
                .andExpect(jsonPath("$", is("Club stats added to league successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenAddClubStatsToLeague_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
                .when(clubStatsService).addClubStatsToLeague(anyLong(), anySet());
        mockMvc.perform(
                MockMvcRequestBuilders.post("/leagues/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(leagueClubStatsDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenUpdateLeague_thenShouldReturnOk() throws Exception {
        doNothing()
                .when(leagueService).updateLeague(anyLong(), any(LeagueDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/leagues/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(leagueDTO)))
                .andExpect(jsonPath("$", is("League updated successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdateLeague_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
                .when(leagueService).updateLeague(anyLong(), any(LeagueDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/leagues/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(leagueDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenDeleteLeague_thenShouldReturnOk() throws Exception {
        doNothing()
                .when(leagueService).deleteLeagueById(anyLong());
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/leagues/1"))
                .andExpect(status().isOk());
    }
}
