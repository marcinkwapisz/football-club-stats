package com.example.project.controller;

import com.example.project.dto.MatchDTO;
import com.example.project.dto.MatchResultDTO;
import com.example.project.dto.MatchToReturnDTO;
import com.example.project.model.Club;
import com.example.project.model.GoalScorer;
import com.example.project.model.League;
import com.example.project.model.Player;
import com.example.project.service.MatchService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MatchController.class)
public class MatchControllerTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    private final LocalDateTime localDateTime = LocalDateTime.of(2020, 9, 25, 12, 45, 30);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private MatchService matchService;
    private MatchToReturnDTO matchToReturnDTO;
    private Page<MatchToReturnDTO> matchToReturnDTOPage;
    private MatchDTO matchDTO;
    private MatchResultDTO matchResultDTO1;
    private MatchResultDTO matchResultDTO2;

    @Before
    public void init() {
        League league = new League();
        league.setId(1L);
        league.setName("League");
        Club club1 = new Club();
        club1.setId(1L);
        club1.setName("Club");
        Club club2 = new Club();
        club2.setId(2L);
        club2.setName("Two");
        Player player = new Player();
        player.setId(1L);
        player.setName("Player");
        GoalScorer goalScorer = new GoalScorer();
        goalScorer.setId(1L);
        goalScorer.setPlayer(player);
        goalScorer.setNumberOfGoals(2);
        matchToReturnDTO = new MatchToReturnDTO();
        matchToReturnDTO.setId(1L);
        matchToReturnDTO.setLocation("Loc");
        matchToReturnDTO.setDate(localDateTime);
        matchToReturnDTO.setLeague(league);
        matchToReturnDTO.setHomeClub(club1);
        matchToReturnDTO.setHomeClubGoals(2);
        matchToReturnDTO.setHomeClubGoalScorers(new ArrayList<>(Collections.singleton(goalScorer)));
        matchToReturnDTO.setAwayClub(club2);
        matchToReturnDTO.setAwayClubGoals(2);
        matchToReturnDTO.setAwayClubGoalScorers(new ArrayList<>(Collections.singleton(goalScorer)));
        List<MatchToReturnDTO> matchToReturnDTOList = new ArrayList<>(Collections.singletonList(matchToReturnDTO));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > matchToReturnDTOList.size() ? matchToReturnDTOList.size() : (start + pageable.getPageSize());
        matchToReturnDTOPage = new PageImpl<>(matchToReturnDTOList.subList((int) start, (int) end), pageable, matchToReturnDTOList.size());
        matchDTO = new MatchDTO();
        matchDTO.setLeague(league);
        matchDTO.setHomeClub(club1);
        matchDTO.setAwayClub(club2);
        matchResultDTO1 = new MatchResultDTO();
        matchResultDTO1.setHomeClubGoals(1);
        matchResultDTO1.setAwayClubGoals(1);
        matchResultDTO2 = new MatchResultDTO();
        matchResultDTO2.setHomeClubGoals(2);
        matchResultDTO2.setHomeClubGoalScorers(new ArrayList<>(Collections.singleton(goalScorer)));
        matchResultDTO2.setAwayClubGoals(2);
        matchResultDTO2.setAwayClubGoalScorers(new ArrayList<>(Collections.singleton(goalScorer)));
    }

    @Test
    public void whenGetAll_thenShouldReturnPageWithMatchToReturnDTOsByPhraseInLocation() throws Exception {
        when(matchService.getMatchesByPhraseInLocation(any(Pageable.class), any(String.class)))
                .thenReturn(matchToReturnDTOPage);
        mockMvc.perform(get("/matches")
                .param("phrase", "loc")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(matchToReturnDTO.getId().intValue())))
                .andExpect(jsonPath("$.content[0].location", is(matchToReturnDTO.getLocation())))
                .andExpect(jsonPath("$.content[0].league.id", is(matchToReturnDTO.getLeague().getId().intValue())))
                .andExpect(jsonPath("$.content[0].league.name", is(matchToReturnDTO.getLeague().getName())))
                .andExpect(jsonPath("$.content[0].date", is(matchToReturnDTO.getDate().toString())))
                .andExpect(jsonPath("$.content[0].homeClub.id", is(matchToReturnDTO.getHomeClub().getId().intValue())))
                .andExpect(jsonPath("$.content[0].homeClub.name", is(matchToReturnDTO.getHomeClub().getName())))
                .andExpect(jsonPath("$.content[0].awayClub.id", is(matchToReturnDTO.getAwayClub().getId().intValue())))
                .andExpect(jsonPath("$.content[0].awayClub.name", is(matchToReturnDTO.getAwayClub().getName())))
                .andExpect(jsonPath("$.content[0].homeClubGoals", is(matchToReturnDTO.getHomeClubGoals())))
                .andExpect(jsonPath("$.content[0].awayClubGoals", is(matchToReturnDTO.getAwayClubGoals())))
                .andExpect(jsonPath("$.content[0].homeClubGoalScorers[0].id",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getId().intValue())))
                .andExpect(jsonPath("$.content[0].homeClubGoalScorers[0].numberOfGoals",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getNumberOfGoals())))
                .andExpect(jsonPath("$.content[0].homeClubGoalScorers[0].player.id",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getPlayer().getId().intValue())))
                .andExpect(jsonPath("$.content[0].homeClubGoalScorers[0].player.name",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getPlayer().getName())))
                .andExpect(jsonPath("$.content[0].awayClubGoalScorers[0].id",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getId().intValue())))
                .andExpect(jsonPath("$.content[0].awayClubGoalScorers[0].numberOfGoals",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getNumberOfGoals())))
                .andExpect(jsonPath("$.content[0].awayClubGoalScorers[0].player.id",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getPlayer().getId().intValue())))
                .andExpect(jsonPath("$.content[0].awayClubGoalScorers[0].player.name",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getPlayer().getName())));
    }

    @Test
    public void whenGetAll_thenShouldReturnNotFound() throws Exception {
        when(matchService.getMatchesByPhraseInLocation(any(Pageable.class), any(String.class)))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/matches")
                .param("phrase", "loc")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetById_thenShouldReturnMatchToReturnDTO() throws Exception {
        when(matchService.getMatchById(anyLong())).thenReturn(matchToReturnDTO);
        mockMvc.perform(get("/matches/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(matchToReturnDTO.getId().intValue())))
                .andExpect(jsonPath("$.location", is(matchToReturnDTO.getLocation())))
                .andExpect(jsonPath("$.league.id", is(matchToReturnDTO.getLeague().getId().intValue())))
                .andExpect(jsonPath("$.league.name", is(matchToReturnDTO.getLeague().getName())))
                .andExpect(jsonPath("$.date", is(matchToReturnDTO.getDate().toString())))
                .andExpect(jsonPath("$.homeClub.id", is(matchToReturnDTO.getHomeClub().getId().intValue())))
                .andExpect(jsonPath("$.homeClub.name", is(matchToReturnDTO.getHomeClub().getName())))
                .andExpect(jsonPath("$.awayClub.id", is(matchToReturnDTO.getAwayClub().getId().intValue())))
                .andExpect(jsonPath("$.awayClub.name", is(matchToReturnDTO.getAwayClub().getName())))
                .andExpect(jsonPath("$.homeClubGoals", is(matchToReturnDTO.getHomeClubGoals())))
                .andExpect(jsonPath("$.awayClubGoals", is(matchToReturnDTO.getAwayClubGoals())))
                .andExpect(jsonPath("$.homeClubGoalScorers[0].id",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getId().intValue())))
                .andExpect(jsonPath("$.homeClubGoalScorers[0].numberOfGoals",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getNumberOfGoals())))
                .andExpect(jsonPath("$.homeClubGoalScorers[0].player.id",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getPlayer().getId().intValue())))
                .andExpect(jsonPath("$.homeClubGoalScorers[0].player.name",
                        is(matchToReturnDTO.getHomeClubGoalScorers().get(0).getPlayer().getName())))
                .andExpect(jsonPath("$.awayClubGoalScorers[0].id",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getId().intValue())))
                .andExpect(jsonPath("$.awayClubGoalScorers[0].numberOfGoals",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getNumberOfGoals())))
                .andExpect(jsonPath("$.awayClubGoalScorers[0].player.id",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getPlayer().getId().intValue())))
                .andExpect(jsonPath("$.awayClubGoalScorers[0].player.name",
                        is(matchToReturnDTO.getAwayClubGoalScorers().get(0).getPlayer().getName())));
    }

    @Test
    public void whenGetById_thenShouldReturnNotFound() throws Exception {
        when(matchService.getMatchById(anyLong())).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/matches/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenAddMatch_thenShouldReturnCreated() throws Exception {
        doNothing().when(matchService).addMatch(any(MatchDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/matches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchDTO)))
                .andExpect(jsonPath("$", is("Match created successfully")))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenUpdateMatch_thenShouldReturnOk() throws Exception {
        doNothing().when(matchService).updateMatch(anyLong(), any(MatchDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchDTO)))
                .andExpect(jsonPath("$", is("Match updated successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdateMatch_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class).when(matchService).updateMatch(anyLong(), any(MatchDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenSetOfGoalScorers_thenShouldReturnOk() throws Exception {
        doNothing().when(matchService).setNumberOfGoals(anyLong(), any(MatchResultDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1/goals")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchResultDTO1)))
                .andExpect(jsonPath("$", is("Number of goals set successfully")))
                .andExpect(status().isOk());

        doNothing().when(matchService).setGoalScorers(anyLong(), any(MatchResultDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1/goals")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchResultDTO2)))
                .andExpect(jsonPath("$", is("Goals scorers and number of goals set successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenSetOfGoalScorers_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class).when(matchService).setNumberOfGoals(anyLong(), any(MatchResultDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1/goals")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchResultDTO1)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenSetOfGoalScorers_thenShouldReturnMethodNotAllowed() throws Exception {
        doThrow(DateTimeException.class).when(matchService).setNumberOfGoals(anyLong(), any(MatchResultDTO.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/matches/1/goals")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(matchResultDTO1)))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void whenDeleteMatch_thenShouldReturnOk() throws Exception {
        doNothing().when(matchService).deleteMatchById(anyLong());
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/matches/1"))
                .andExpect(status().isOk());
    }
}
