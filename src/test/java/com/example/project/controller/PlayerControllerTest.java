package com.example.project.controller;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.Club;
import com.example.project.model.Player;
import com.example.project.service.PlayerService;
import com.example.project.service.PlayerStatsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PlayerController.class)
public class PlayerControllerTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PlayerService playerService;
    @MockBean
    private PlayerStatsService playerStatsService;
    private Player player;
    private Club club;
    private Page<Player> playerPage;
    @Mock
    private PlayerStatsDTO playerStatsDTO;
    private Set<PlayerStatsDTO> playerStatsDTOSet;

    @Before
    public void init() {
        player = new Player();
        player.setId(1L);
        player.setName("Player");
        club = new Club();
        club.setId(1L);
        club.setName("One");
        player.setClub(club);
        List<Player> playerList = new ArrayList<>(Collections.singletonList(player));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > playerList.size() ? playerList.size() : (start + pageable.getPageSize());
        playerPage = new PageImpl<>(playerList.subList((int) start, (int) end), pageable, playerList.size());
        playerStatsDTOSet = new HashSet<>(Collections.singletonList(playerStatsDTO));
    }

    @Test
    public void whenGetAll_thenShouldReturnPageWithPlayersByPhraseInName() throws Exception {
        when(playerService.getPlayersByPhraseInName(any(Pageable.class), any(String.class)))
                .thenReturn(playerPage);
        mockMvc.perform(get("/players")
                .param("phrase", "pla")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].id", is(player.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(player.getName())))
                .andExpect(jsonPath("$.content[0].club.id", is(club.getId().intValue())))
                .andExpect(jsonPath("$.content[0].club.name", is(club.getName())));
    }

    @Test
    public void whenGetAll_thenShouldReturnNotFound() throws Exception {
        when(playerService.getPlayersByPhraseInName(any(Pageable.class), any(String.class)))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/players")
                .param("phrase", "tw")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetById_thenShouldReturnPlayer() throws Exception {
        when(playerService.getPlayerById(anyLong())).thenReturn(player);
        mockMvc.perform(get("/players/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(player.getId().intValue())))
                .andExpect(jsonPath("$.name", is(player.getName())))
                .andExpect(jsonPath("$.club.id", is(club.getId().intValue())))
                .andExpect(jsonPath("$.club.name", is(club.getName())));
    }

    @Test
    public void whenGetById_thenShouldReturnNotFound() throws Exception {
        when(playerService.getPlayerById(anyLong())).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/players/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenGetPlayerStatsByPlayerId_thenShouldReturnPlayerStatsDTOSet() throws Exception {
        when(playerStatsService.getSetOfPlayerStatsByPlayerId(anyLong()))
                .thenReturn(playerStatsDTOSet);
        mockMvc.perform(get("/players/1/playerStats")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].leagueName", is(playerStatsDTO.getLeagueName())))
                .andExpect(jsonPath("$[0].numberOfHomeGoals", is(playerStatsDTO.getNumberOfHomeGoals())))
                .andExpect(jsonPath("$[0].numberOfAwayGoals", is(playerStatsDTO.getNumberOfAwayGoals())));
    }

    @Test
    public void whenGetPlayerStatsByPlayerId_thenShouldReturnNotFound() throws Exception {
        when(playerStatsService.getSetOfPlayerStatsByPlayerId(anyLong()))
                .thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/players/1/playerStats")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenAddPlayer_thenShouldReturnCreated() throws Exception {
        doNothing()
                .when(playerService).addPlayer(any(Player.class));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/players")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(player)))
                .andExpect(jsonPath("$", is("Player created successfully")))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenUpdatePlayer_thenShouldReturnOk() throws Exception {
        doNothing()
                .when(playerService).updatePlayer(anyLong(), any(Player.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/players/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(player)))
                .andExpect(jsonPath("$", is("Player updated successfully")))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUpdatePlayer_thenShouldReturnNotFound() throws Exception {
        doThrow(EntityNotFoundException.class)
                .when(playerService).updatePlayer(anyLong(), any(Player.class));
        mockMvc.perform(
                MockMvcRequestBuilders.put("/players/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(player)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenDeletePlayer_thenShouldReturnOk() throws Exception {
        doNothing()
                .when(playerService).deletePlayerById(anyLong());
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/players/1"))
                .andExpect(status().isOk());
    }
}
