package com.example.project.repository;

import com.example.project.model.Club;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ClubRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClubRepository clubRepository;

    @Before
    public void init() {
        Club club1 = new Club();
        club1.setName("One");
        entityManager.persist(club1);
        Club club2 = new Club();
        club2.setName("Two");
        entityManager.persist(club2);
        entityManager.flush();
    }

    @Test
    public void whenFindById_thenReturnClubOrOptionalEmpty() {
        Optional<Club> foundClub = clubRepository.findById(1L);
        assertFalse(foundClub.isEmpty());
        assertThat(foundClub.get().getId() == 1L);

        Optional<Club> notFoundClub = clubRepository.findById(100L);
        assertTrue(notFoundClub.isEmpty());
    }

    @Test
    public void whenFindByNameContainingIgnoreCase_thenReturnClubPage() {
        Pageable pageable = PageRequest.of(0, 10);
        Optional<Page<Club>> foundOneClub = clubRepository.findByNameContainingIgnoreCase(pageable, "One");
        assertFalse(foundOneClub.isEmpty());
        assertThat(foundOneClub.get().getTotalElements() == 1);
        assertThat(foundOneClub.get().toList().get(0).getName().equals("One"));

        Optional<Page<Club>> foundTwoClubs = clubRepository.findByNameContainingIgnoreCase(pageable, "o");
        assertFalse(foundTwoClubs.isEmpty());
        assertThat(foundTwoClubs.get().getTotalElements() == 2);
        assertThat(foundTwoClubs.get().toList().get(0).getName().equals("One"));
        assertThat(foundTwoClubs.get().toList().get(1).getName().equals("Two"));

        Optional<Page<Club>> notFoundClubs = clubRepository.findByNameContainingIgnoreCase(pageable, "Not found");
        assertTrue(notFoundClubs.get().isEmpty());
        assertThat(notFoundClubs.get().getTotalElements() == 0);
    }
}
