package com.example.project.repository;

import com.example.project.model.Club;
import com.example.project.model.ClubStats;
import com.example.project.model.League;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ClubStatsRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClubStatsRepository clubStatsRepository;

    @Before
    public void init() {
        League league1 = new League();
        entityManager.persist(league1);
        Club club1 = new Club();
        entityManager.persist(club1);
        entityManager.flush();

        ClubStats clubStats1 = new ClubStats();
        clubStats1.setClub(club1);
        clubStats1.setLeague(league1);
        entityManager.persistAndFlush(clubStats1);
    }

    @Test
    public void whenFindByLeagueIdAndClubId_thenReturnClubStatsOrNull() {
        ClubStats foundClubStats = clubStatsRepository.findByLeagueIdAndClubId(1L, 1L);
        assertThat(foundClubStats).isNotNull();
        assertThat(foundClubStats.getId() == 1L);
        assertThat(foundClubStats.getClub().getId() == 1L);
        assertThat(foundClubStats.getLeague().getId() == 1L);

        ClubStats notFoundClubStats1 = clubStatsRepository.findByLeagueIdAndClubId(100L, 1L);
        assertThat(notFoundClubStats1).isNull();

        ClubStats notFoundClubStats2 = clubStatsRepository.findByLeagueIdAndClubId(1L, 100L);
        assertThat(notFoundClubStats2).isNull();
    }
}
