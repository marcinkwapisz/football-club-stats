package com.example.project.repository;

import com.example.project.model.GoalScorer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GoalScorerRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GoalScorerRepository goalScorerRepository;

    @Before
    public void init() {
        GoalScorer goalScorer1 = new GoalScorer();
        entityManager.persist(goalScorer1);
        GoalScorer goalScorer2 = new GoalScorer();
        entityManager.persist(goalScorer2);
        entityManager.flush();
    }

    @Test
    public void whenFindById_thenReturnGoalScorerOrOptionalEmpty() {
        Optional<GoalScorer> foundGoalScorer = goalScorerRepository.findById(1L);
        assertFalse(foundGoalScorer.isEmpty());
        assertThat(foundGoalScorer.get().getId() == 1L);

        Optional<GoalScorer> foundGoalScorer2 = goalScorerRepository.findById(1L);
        assertFalse(foundGoalScorer2.isEmpty());
        assertThat(foundGoalScorer2.get().getId() == 2L);

        Optional<GoalScorer> notFoundGoalScorer = goalScorerRepository.findById(100L);
        assertTrue(notFoundGoalScorer.isEmpty());
    }
}
