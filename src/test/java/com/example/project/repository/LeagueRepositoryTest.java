package com.example.project.repository;

import com.example.project.model.League;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class LeagueRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LeagueRepository leagueRepository;

    @Before
    public void init() {
        League league1 = new League();
        league1.setName("One");
        entityManager.persist(league1);
        League league2 = new League();
        league2.setName("Two");
        entityManager.persist(league2);
        entityManager.flush();
    }

    @Test
    public void whenFindById_thenReturnLeagueOrOptionalEmpty() {
        Optional<League> foundLeague = leagueRepository.findById(1L);
        assertFalse(foundLeague.isEmpty());
        assertThat(foundLeague.get().getId() == 1L);

        Optional<League> notFoundLeague = leagueRepository.findById(100L);
        assertTrue(notFoundLeague.isEmpty());
    }

    @Test
    public void whenFindByNameContainingIgnoreCase_thenReturnLeaguePage() {
        Pageable pageable = PageRequest.of(0, 10);
        Optional<Page<League>> foundOneLeague = leagueRepository.findByNameContainingIgnoreCase(pageable, "One");
        assertFalse(foundOneLeague.isEmpty());
        assertThat(foundOneLeague.get().getTotalElements() == 1);
        assertThat(foundOneLeague.get().toList().get(0).getName().equals("One"));

        Optional<Page<League>> foundTwoLeagues = leagueRepository.findByNameContainingIgnoreCase(pageable, "o");
        assertFalse(foundTwoLeagues.isEmpty());
        assertThat(foundTwoLeagues.get().getTotalElements() == 2);
        assertThat(foundTwoLeagues.get().toList().get(0).getName().equals("One"));
        assertThat(foundTwoLeagues.get().toList().get(1).getName().equals("Two"));

        Optional<Page<League>> notFoundLeagues = leagueRepository.findByNameContainingIgnoreCase(pageable, "Not found");
        assertTrue(notFoundLeagues.get().isEmpty());
        assertThat(notFoundLeagues.get().getTotalElements() == 0);
    }
}
