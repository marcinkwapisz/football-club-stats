package com.example.project.repository;

import com.example.project.model.Match;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MatchRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MatchRepository matchRepository;

    @Before
    public void init() {
        Match match1 = new Match();
        match1.setLocation("One");
        entityManager.persist(match1);
        Match match2 = new Match();
        match2.setLocation("Two");
        entityManager.persist(match2);
        entityManager.flush();
    }

    @Test
    public void whenFindById_thenReturnMatchOrOptionalEmpty() {
        Optional<Match> foundMatch = matchRepository.findById(1L);
        assertFalse(foundMatch.isEmpty());
        assertThat(foundMatch.get().getId() == 1L);

        Optional<Match> notFoundMatch = matchRepository.findById(100L);
        assertTrue(notFoundMatch.isEmpty());
    }

    @Test
    public void whenFindByLocationContainingIgnoreCase_thenReturnMatchPage() {
        Pageable pageable = PageRequest.of(0, 10);
        Optional<Page<Match>> foundOneMatch = matchRepository.findByLocationContainingIgnoreCase(pageable, "One");
        assertFalse(foundOneMatch.isEmpty());
        assertThat(foundOneMatch.get().getTotalElements() == 1);
        assertThat(foundOneMatch.get().toList().get(0).getLocation().equals("One"));

        Optional<Page<Match>> foundTwoMatches = matchRepository.findByLocationContainingIgnoreCase(pageable, "o");
        assertFalse(foundTwoMatches.isEmpty());
        assertThat(foundTwoMatches.get().getTotalElements() == 2);
        assertThat(foundTwoMatches.get().toList().get(0).getLocation().equals("One"));
        assertThat(foundTwoMatches.get().toList().get(1).getLocation().equals("Two"));

        Optional<Page<Match>> notFoundMatches = matchRepository.findByLocationContainingIgnoreCase(pageable, "Not found");
        assertTrue(notFoundMatches.get().isEmpty());
        assertThat(notFoundMatches.get().getTotalElements() == 0);
    }
}
