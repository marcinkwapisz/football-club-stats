package com.example.project.repository;

import com.example.project.model.Player;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlayerRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PlayerRepository playerRepository;

    @Before
    public void init() {
        Player player1 = new Player();
        player1.setName("One");
        entityManager.persist(player1);
        Player player2 = new Player();
        player2.setName("Two");
        entityManager.persist(player2);
        Player player3 = new Player();
        player3.setName("Three");
        entityManager.persist(player3);
        entityManager.flush();
    }

    @Test
    public void whenFindById_thenReturnPlayerOrOptionalEmpty() {
        Optional<Player> foundPlayer = playerRepository.findById(1L);
        assertFalse(foundPlayer.isEmpty());
        assertThat(foundPlayer.get().getId() == 1L);

        Optional<Player> notFoundPlayer = playerRepository.findById(100L);
        assertTrue(notFoundPlayer.isEmpty());
    }

    @Test
    public void whenFindByNameContainingIgnoreCase_thenReturnPlayerPage() {
        Pageable pageable = PageRequest.of(0, 10);
        Optional<Page<Player>> foundOnePlayer = playerRepository.findByNameContainingIgnoreCase(pageable, "One");
        assertFalse(foundOnePlayer.isEmpty());
        assertThat(foundOnePlayer.get().getTotalElements() == 1);
        assertThat(foundOnePlayer.get().toList().get(0).getName().equals("One"));

        Optional<Page<Player>> foundTwoPlayers = playerRepository.findByNameContainingIgnoreCase(pageable, "t");
        assertFalse(foundTwoPlayers.isEmpty());
        assertThat(foundTwoPlayers.get().getTotalElements() == 2);
        assertThat(foundTwoPlayers.get().toList().get(0).getName().equals("Two"));
        assertThat(foundTwoPlayers.get().toList().get(1).getName().equals("Three"));

        Optional<Page<Player>> foundAllPlayers = playerRepository.findByNameContainingIgnoreCase(pageable, "");
        assertFalse(foundAllPlayers.isEmpty());
        assertThat(foundAllPlayers.get().getTotalElements() == 3);
        assertThat(foundAllPlayers.get().toList().get(0).getName().equals("One"));
        assertThat(foundAllPlayers.get().toList().get(1).getName().equals("Two"));
        assertThat(foundAllPlayers.get().toList().get(2).getName().equals("Three"));

        Optional<Page<Player>> notFoundPlayers = playerRepository.findByNameContainingIgnoreCase(pageable, "Not found");
        assertTrue(notFoundPlayers.get().isEmpty());
        assertThat(notFoundPlayers.get().getTotalElements() == 0);
    }
}
