package com.example.project.repository;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.League;
import com.example.project.model.Player;
import com.example.project.model.PlayerStats;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlayerStatsRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PlayerStatsRepository playerStatsRepository;

    @Before
    public void init() {
        League league1 = new League();
        entityManager.persist(league1);
        Player player1 = new Player();
        entityManager.persist(player1);
        entityManager.flush();

        PlayerStats playerStats1 = new PlayerStats();
        playerStats1.setPlayer(player1);
        playerStats1.setLeague(league1);
        entityManager.persistAndFlush(playerStats1);
    }

    @Test
    public void whenFindByLeagueIdAndPlayerId_thenReturnPlayerStatsOrNull() {
        PlayerStats foundPlayerStats = playerStatsRepository.findByLeagueIdAndPlayerId(1L, 1L);
        assertThat(foundPlayerStats).isNotNull();
        assertThat(foundPlayerStats.getId() == 1L);
        assertThat(foundPlayerStats.getPlayer().getId() == 1L);
        assertThat(foundPlayerStats.getLeague().getId() == 1L);

        PlayerStats notFoundPlayerStats1 = playerStatsRepository.findByLeagueIdAndPlayerId(100L, 1L);
        assertThat(notFoundPlayerStats1).isNull();

        PlayerStats notFoundPlayerStats2 = playerStatsRepository.findByLeagueIdAndPlayerId(1L, 100L);
        assertThat(notFoundPlayerStats2).isNull();
    }

    @Test
    public void whenFindSetOfPlayerStatsByPlayerId_thenReturnPlayerStatsDTOSet() {
        Set<PlayerStatsDTO> foundPlayerStats = playerStatsRepository.findSetOfPlayerStatsByPlayerId(1L);
        assertThat(foundPlayerStats).isNotNull();
        assertThat(foundPlayerStats.size() == 1);

        Set<PlayerStatsDTO> notFoundPlayerStats = playerStatsRepository.findSetOfPlayerStatsByPlayerId(100L);
        assertThat(notFoundPlayerStats).isNotNull();
        assertThat(notFoundPlayerStats.isEmpty());
    }
}
