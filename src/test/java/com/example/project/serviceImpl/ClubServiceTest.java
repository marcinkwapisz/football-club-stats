package com.example.project.serviceImpl;

import com.example.project.dto.ClubDTO;
import com.example.project.model.Club;
import com.example.project.model.Player;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.PlayerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClubServiceTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private ClubRepository clubRepository;
    @InjectMocks
    private ClubServiceImpl clubService;

    private Club clubWithoutPlayers;
    private Club clubWithPlayers;
    private Page<Club> clubPage;
    private ClubDTO clubDTO;
    private List<Player> playerList;

    @Before
    public void init() {
        Player player1 = new Player();
        player1.setId(1L);
        Player player2 = new Player();
        player2.setId(2L);
        playerList = new ArrayList<>(Arrays.asList(player1, player2));
        clubWithoutPlayers = new Club();
        clubWithoutPlayers.setName("One");
        clubWithPlayers = new Club();
        clubWithPlayers.setName("Two");
        clubWithPlayers.setPlayers(playerList);
        List<Club> clubList = new ArrayList<>(Arrays.asList(clubWithoutPlayers, clubWithPlayers));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > clubList.size() ? clubList.size() : (start + pageable.getPageSize());
        clubPage = new PageImpl<>(clubList.subList((int) start, (int) end), pageable, clubList.size());
        clubDTO = new ClubDTO();
        clubDTO.setName("One");
    }

    @Test
    public void whenGetClubsByPhraseInName_thenReturnClubDTOPage() {
        when(clubRepository.findByNameContainingIgnoreCase(any(Pageable.class), any(String.class)))
                .thenReturn(Optional.of(clubPage));
        Page<ClubDTO> clubDTOPage = clubService.getClubsByPhraseInName(pageable, "o");
        assertThat(clubDTOPage.getTotalElements()).isEqualTo(clubPage.getTotalElements());
        assertThat(clubDTOPage.getContent().get(0).getName())
                .isEqualTo(clubPage.getContent().get(0).getName());
        assertThat(clubDTOPage.getContent().get(0).getId())
                .isEqualTo(clubPage.getContent().get(0).getId());
        assertThat(clubDTOPage.getContent().get(1).getName())
                .isEqualTo(clubPage.getContent().get(1).getName());
        assertThat(clubDTOPage.getContent().get(1).getId())
                .isEqualTo(clubPage.getContent().get(1).getId());
    }

    @Test
    public void whenGetClubsByPhraseInName_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> clubService.getClubsByPhraseInName(pageable, "o"));
        assertEquals(exception.getMessage(), "No clubs were found associating with this phrase");
    }

    @Test
    public void whenGetClubById_thenReturnClub() {
        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubWithoutPlayers));
        Club foundClub = clubService.getClubById(1L);
        assertThat(foundClub.getId()).isEqualTo(clubWithoutPlayers.getId());
        assertThat(foundClub.getName()).isEqualTo(clubWithoutPlayers.getName());
        assertThat(foundClub.getPlayers()).isEqualTo(clubWithoutPlayers.getPlayers());
    }

    @Test
    public void whenGetClubById_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> clubService.getClubById(1L));
        assertEquals(exception.getMessage(), "Club with given id does not exist");
    }

    @Test
    public void whenAddClubDTO_thenSaveClubInRepository() {
        when(clubRepository.save(any(Club.class))).thenAnswer(i -> i.getArguments()[0]);
        clubService.addClub(clubDTO);
    }

    @Test
    public void whenUpdateClubDTO_thenSaveClubInRepository() {
        when(clubRepository.save(any(Club.class))).thenAnswer(i -> i.getArguments()[0]);
        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubPage.getContent().get(0)));
        clubService.updateClub(1L, clubDTO);
    }

    @Test
    public void whenUpdateClub_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> clubService.updateClub(1L, clubDTO));
        assertEquals(exception.getMessage(), "Club with given id does not exist");
    }

    @Test
    public void whenUpdateListOfPlayersInClub_thenSaveClubInRepository() {
        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubWithoutPlayers));
        when(clubRepository.save(any(Club.class))).thenAnswer(i -> i.getArguments()[0]);
        clubService.updateListOfPlayersInClub(1L, playerList);
        assertThat(clubWithoutPlayers.getPlayers()).isNull();

        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubWithPlayers));
        when(playerRepository.findById(any(Long.class))).thenReturn(Optional.of(playerList.get(0)));
        when(playerRepository.save(any(Player.class))).thenAnswer(i -> i.getArguments()[0]);
        clubService.updateListOfPlayersInClub(1L, playerList);
        assertThat(clubWithPlayers.getPlayers()).isNotNull();
    }

    @Test
    public void whenUpdateListOfPlayersInClub_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> clubService.updateClub(1L, clubDTO));
        assertEquals(exception.getMessage(), "Club with given id does not exist");
    }

    @Test
    public void whenDeleteClubById_thenDeleteClubFromRepository() {
        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubWithoutPlayers));
        doNothing().when(clubRepository).delete(any(Club.class));
        clubService.deleteClubById(1L);

        when(clubRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubWithPlayers));
        when(playerRepository.save(any(Player.class))).thenAnswer(i -> i.getArguments()[0]);
        clubService.deleteClubById(2L);
    }
}
