package com.example.project.serviceImpl;

import com.example.project.model.Club;
import com.example.project.model.ClubStats;
import com.example.project.model.League;
import com.example.project.model.Match;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.ClubStatsRepository;
import com.example.project.repository.LeagueRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClubStatsServiceTest {
    @Mock
    private ClubStatsRepository clubStatsRepository;

    @Mock
    private LeagueRepository leagueRepository;

    @Mock
    private ClubRepository clubRepository;

    @InjectMocks
    private ClubStatsServiceImpl clubStatsService;

    private League league;
    private Club club1;
    private ClubStats clubStats1;
    private ClubStats clubStats2;
    private Match match1;
    private Match match2;

    @Before
    public void init() {
        league = new League();
        league.setId(1L);
        club1 = new Club();
        club1.setId(1L);
        Club club2 = new Club();
        club2.setId(2L);
        clubStats1 = new ClubStats();
        clubStats1.setId(1L);
        clubStats1.setClub(club1);
        clubStats2 = new ClubStats();
        clubStats2.setId(2L);
        clubStats2.setClub(club2);
        match1 = new Match();
        match1.setLeague(league);
        match1.setHomeClub(club1);
        match1.setAwayClub(club2);
        match1.setHomeClubGoals(2);
        match1.setAwayClubGoals(1);
        match2 = new Match();
        match2.setLeague(league);
        match2.setHomeClub(club1);
        match2.setAwayClub(club2);
        match2.setHomeClubGoals(1);
        match2.setAwayClubGoals(1);
    }

    @Test
    public void whenAddClubStatsToLeague_thenSaveLeagueInRepository() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league));
        when(clubRepository.getOne(any(Long.class))).thenReturn(club1);
        when(clubStatsRepository.save(any(ClubStats.class))).thenReturn(clubStats1);
        when(leagueRepository.save(any(League.class))).thenReturn(league);
        assertFalse(league.getScoreboard().contains(clubStats1));
        clubStatsService.addClubStatsToLeague(1L, new HashSet<>(Collections.singletonList(clubStats2)));
        assertTrue(league.getScoreboard().contains(clubStats1));
    }

    @Test
    public void whenAddClubStatsToLeague_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class, () ->
                clubStatsService.addClubStatsToLeague(1L, new HashSet<>(Collections.singletonList(clubStats2))));
        assertEquals(exception.getMessage(), "League with given id does not exist");
    }

    @Test
    public void whenUpdateClubStatsAfterMatch_thenSaveClubStatsInRepository() {
        when(clubStatsRepository.findByLeagueIdAndClubId(anyLong(), anyLong())).thenReturn(clubStats1);
        when(clubStatsRepository.save(any(ClubStats.class))).thenAnswer(i -> i.getArguments()[0]);
        clubStatsService.updateClubStatsAfterMatch(match1);
        assertEquals(2, clubStats1.getNumberOfMatches());
        assertEquals(1, clubStats1.getNumberOfMatchesWon());
        assertEquals(0, clubStats1.getNumberOfMatchesTied());
        assertEquals(1, clubStats1.getNumberOfMatchesLost());
        assertEquals(3, clubStats1.getNumberOfGoalsScored());
        assertEquals(3, clubStats1.getNumberOfGoalsConceded());
        assertEquals(0, clubStats1.getGoalDifference());
        assertEquals(3, clubStats1.getNumberOfPoints());

        when(clubStatsRepository.findByLeagueIdAndClubId(anyLong(), anyLong())).thenReturn(clubStats2);
        clubStatsService.updateClubStatsAfterMatch(match2);
        assertEquals(2, clubStats2.getNumberOfMatches());
        assertEquals(0, clubStats2.getNumberOfMatchesWon());
        assertEquals(2, clubStats2.getNumberOfMatchesTied());
        assertEquals(0, clubStats2.getNumberOfMatchesLost());
        assertEquals(2, clubStats2.getNumberOfGoalsScored());
        assertEquals(2, clubStats2.getNumberOfGoalsConceded());
        assertEquals(0, clubStats2.getGoalDifference());
        assertEquals(2, clubStats2.getNumberOfPoints());
    }

    @Test
    public void whenClearClubStatsBeforeMatchUpdate_thenSaveClubStatsInRepository() {
        when(clubStatsRepository.findByLeagueIdAndClubId(anyLong(), anyLong())).thenReturn(clubStats1);
        when(clubStatsRepository.save(any(ClubStats.class))).thenAnswer(i -> i.getArguments()[0]);
        clubStatsService.updateClubStatsAfterMatch(match1);
        clubStatsService.clearClubStatsBeforeMatchUpdate(match1);
        assertEquals(0, clubStats1.getNumberOfMatches());
        assertEquals(0, clubStats1.getNumberOfMatchesWon());
        assertEquals(0, clubStats1.getNumberOfMatchesTied());
        assertEquals(0, clubStats1.getNumberOfMatchesLost());
        assertEquals(0, clubStats1.getNumberOfGoalsScored());
        assertEquals(0, clubStats1.getNumberOfGoalsConceded());
        assertEquals(0, clubStats1.getGoalDifference());
        assertEquals(0, clubStats1.getNumberOfPoints());

        when(clubStatsRepository.findByLeagueIdAndClubId(anyLong(), anyLong())).thenReturn(clubStats2);
        clubStatsService.updateClubStatsAfterMatch(match2);
        clubStatsService.clearClubStatsBeforeMatchUpdate(match2);
        assertEquals(0, clubStats2.getNumberOfMatches());
        assertEquals(0, clubStats2.getNumberOfMatchesWon());
        assertEquals(0, clubStats2.getNumberOfMatchesTied());
        assertEquals(0, clubStats2.getNumberOfMatchesLost());
        assertEquals(0, clubStats2.getNumberOfGoalsScored());
        assertEquals(0, clubStats2.getNumberOfGoalsConceded());
        assertEquals(0, clubStats2.getGoalDifference());
        assertEquals(0, clubStats2.getNumberOfPoints());
    }

    @Test
    public void whenDeleteClubStats_thenDeleteClubStatsFromRepository() {
        when(clubStatsRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(clubStats1));
        doNothing().when(clubStatsRepository).delete(any(ClubStats.class));
        clubStatsService.deleteClubStats(1L);
    }
}
