package com.example.project.serviceImpl;

import com.example.project.dto.LeagueDTO;
import com.example.project.dto.MatchScheduleDTO;
import com.example.project.model.League;
import com.example.project.model.Match;
import com.example.project.model.Player;
import com.example.project.model.PlayerStats;
import com.example.project.repository.LeagueRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LeagueServiceTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    private final LocalDateTime localDateTime = LocalDateTime.of(2020, 9, 25, 12, 45);
    @Mock
    private LeagueRepository leagueRepository;
    @InjectMocks
    private LeagueServiceImpl leagueService;
    private Page<League> leaguePage;
    private League league1;
    private League league2;
    private Match match1;
    private LeagueDTO leagueDTO1;
    private PlayerStats playerStats1;

    @Before
    public void init() {
        league1 = new League();
        league1.setId(1L);
        league1.setName("One");
        league2 = new League();
        league2.setId(2L);
        league2.setName("Two");
        league2.setMatchSchedule(new HashSet<>());
        List<League> leagueList = new ArrayList<>(Arrays.asList(league1, league2));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > leagueList.size() ? leagueList.size() : (start + pageable.getPageSize());
        leaguePage = new PageImpl<>(leagueList.subList((int) start, (int) end), pageable, leagueList.size());
        match1 = new Match();
        match1.setId(1L);
        match1.setLeague(league1);
        match1.setDate(localDateTime);
        match1.setLocation("location");
        league1.setMatchSchedule(new HashSet<>(Collections.singletonList(match1)));
        match1.setHomeClubGoals(1);
        match1.setAwayClubGoals(0);
        Player player1 = new Player();
        player1.setId(1L);
        playerStats1 = new PlayerStats();
        playerStats1.setId(1L);
        playerStats1.setPlayer(player1);
        playerStats1.setNumberOfHomeGoals(1);
        playerStats1.setNumberOfAwayGoals(2);
        playerStats1.setLeague(league1);
        league1.setGoalScorers(new TreeSet<>(Collections.singletonList(playerStats1)));
        leagueDTO1 = new LeagueDTO();
        leagueDTO1.setName("Name");
    }

    @Test
    public void whenGetLeaguesByPhraseInName_thenReturnLeaguePage() {
        when(leagueRepository.findByNameContainingIgnoreCase(any(Pageable.class), any(String.class)))
                .thenReturn(Optional.of(leaguePage));
        Page<League> returnedLeaguePage = leagueService.getLeaguesByPhraseInName(pageable, "o");
        assertThat(returnedLeaguePage.getTotalElements())
                .isEqualTo(leaguePage.getTotalElements());
        assertThat(returnedLeaguePage.getContent().get(0).getId())
                .isEqualTo(leaguePage.getContent().get(0).getId());
        assertThat(returnedLeaguePage.getContent().get(0).getName())
                .isEqualTo(leaguePage.getContent().get(0).getName());
        assertThat(returnedLeaguePage.getContent().get(0).getScoreboard())
                .isEqualTo(leaguePage.getContent().get(0).getScoreboard());
        assertThat(returnedLeaguePage.getContent().get(0).getGoalScorers())
                .isEqualTo(leaguePage.getContent().get(0).getGoalScorers());
        assertThat(returnedLeaguePage.getContent().get(0).getMatchSchedule())
                .isEqualTo(leaguePage.getContent().get(0).getMatchSchedule());
        assertThat(returnedLeaguePage.getContent().get(1).getId())
                .isEqualTo(leaguePage.getContent().get(1).getId());
        assertThat(returnedLeaguePage.getContent().get(1).getName())
                .isEqualTo(leaguePage.getContent().get(1).getName());
        assertThat(returnedLeaguePage.getContent().get(1).getScoreboard())
                .isEqualTo(leaguePage.getContent().get(1).getScoreboard());
        assertThat(returnedLeaguePage.getContent().get(1).getGoalScorers())
                .isEqualTo(leaguePage.getContent().get(1).getGoalScorers());
        assertThat(returnedLeaguePage.getContent().get(1).getMatchSchedule())
                .isEqualTo(leaguePage.getContent().get(1).getMatchSchedule());
    }

    @Test
    public void whenGetLeaguesByPhraseInName_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> leagueService.getLeaguesByPhraseInName(pageable, "o"));
        assertEquals(exception.getMessage(), "No leagues were found associating with this phrase");
    }

    @Test
    public void whenGetLeagueById_thenReturnLeague() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league1));
        League foundLeague = leagueService.getLeagueById(1L);
        assertThat(foundLeague.getId()).isEqualTo(league1.getId());
        assertThat(foundLeague.getName()).isEqualTo(league1.getName());
        assertThat(foundLeague.getScoreboard()).isEqualTo(league1.getScoreboard());
        assertThat(foundLeague.getGoalScorers()).isEqualTo(league1.getGoalScorers());
        assertThat(foundLeague.getMatchSchedule()).isEqualTo(league1.getMatchSchedule());
    }

    @Test
    public void whenGetLeagueById_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> leagueService.getLeagueById(1L));
        assertEquals(exception.getMessage(), "League with given id does not exist");
    }

    @Test
    public void whenGetMatchScheduleByLeagueId_thenReturnMatchScheduleDTOTreeSet() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league1));
        TreeSet<MatchScheduleDTO> matchScheduleDTOS = leagueService.getMatchScheduleByLeagueId(1L);
        assertThat(matchScheduleDTOS.size()).isEqualTo(1);
        MatchScheduleDTO foundMatchScheduleDTO = matchScheduleDTOS.iterator().next();
        assertEquals(match1.getId(), foundMatchScheduleDTO.getId());
        assertEquals(match1.getLocation(), foundMatchScheduleDTO.getLocation());
        assertEquals(match1.getDate(), foundMatchScheduleDTO.getDate());
        assertEquals(match1.getLeague(), foundMatchScheduleDTO.getLeague());
        assertEquals(match1.getHomeClub(), foundMatchScheduleDTO.getHomeClub());
        assertEquals(match1.getAwayClub(), foundMatchScheduleDTO.getAwayClub());
        assertEquals(match1.getHomeClubGoals(), foundMatchScheduleDTO.getHomeClubGoals());
        assertEquals(match1.getAwayClubGoals(), foundMatchScheduleDTO.getAwayClubGoals());
    }

    @Test
    public void whenGetPlayerStatsByLeagueId_thenReturnPlayerStatsSet() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league1));
        Set<PlayerStats> playerStatsByLeagueId = leagueService.getPlayerStatsByLeagueId(1L);
        assertEquals(1, playerStatsByLeagueId.size());
        PlayerStats foundPlayerStats = playerStatsByLeagueId.iterator().next();
        assertEquals(playerStats1.getId(), foundPlayerStats.getId());
        assertEquals(playerStats1.getPlayer(), foundPlayerStats.getPlayer());
        assertEquals(playerStats1.getLeague(), foundPlayerStats.getLeague());
        assertEquals(playerStats1.getNumberOfHomeGoals(), foundPlayerStats.getNumberOfHomeGoals());
        assertEquals(playerStats1.getNumberOfAwayGoals(), foundPlayerStats.getNumberOfAwayGoals());
    }

    @Test
    public void whenAddLeague_thenSaveLeagueInRepository() {
        when(leagueRepository.save(any(League.class))).thenAnswer(i -> i.getArguments()[0]);
        leagueService.addLeague(leagueDTO1);
    }

    @Test
    public void whenUpdateLeague_thenSaveLeagueInRepository() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league1));
        when(leagueRepository.save(any(League.class))).thenAnswer(i -> i.getArguments()[0]);
        leagueService.updateLeague(1L, leagueDTO1);
    }

    @Test
    public void whenUpdateLeague_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> leagueService.updateLeague(1L, leagueDTO1));
        assertEquals(exception.getMessage(), "League with given id does not exist");
    }

    @Test
    public void whenUpdateMatchSchedule_thenSaveLeagueInRepository() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league2));
        when(leagueRepository.save(any(League.class))).thenAnswer(i -> i.getArguments()[0]);
        leagueService.updateMatchSchedule(match1);
    }

    @Test
    public void whenUpdateMatchSchedule_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> leagueService.updateMatchSchedule(match1));
        assertEquals(exception.getMessage(), "League with given id does not exist");
    }

    @Test
    public void whenDeleteLeagueById_thenDeleteLeagueFromRepository() {
        when(leagueRepository.findById(any(Long.class))).thenReturn(Optional.of(league2));
        doNothing().when(leagueRepository).delete(any(League.class));
        leagueService.deleteLeagueById(1L);
    }
}
