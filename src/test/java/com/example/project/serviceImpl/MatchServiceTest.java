package com.example.project.serviceImpl;

import com.example.project.dto.MatchDTO;
import com.example.project.dto.MatchResultDTO;
import com.example.project.dto.MatchToReturnDTO;
import com.example.project.model.*;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.GoalScorerRepository;
import com.example.project.repository.MatchRepository;
import com.example.project.repository.PlayerRepository;
import com.example.project.service.ClubStatsService;
import com.example.project.service.LeagueService;
import com.example.project.service.PlayerStatsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MatchServiceTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    private final LocalDateTime localDateTime = LocalDateTime.of(2020, 9, 25, 12, 45);
    @Mock
    private MatchRepository matchRepository;
    @Mock
    private ClubRepository clubRepository;
    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private GoalScorerRepository goalScorerRepository;
    @Mock
    private ClubStatsService clubStatsService;
    @Mock
    private LeagueService leagueService;
    @Mock
    private PlayerStatsService playerStatsService;
    @InjectMocks
    private MatchServiceImpl matchService;

    private Page<Match> matchPage;
    private Match match1;
    private Match match2;
    private League league;
    private Club club1;
    private MatchDTO matchDTO;
    private MatchResultDTO matchResultDTO;
    private Player player1;

    @Before
    public void init() {
        league = new League();
        league.setId(1L);
        league.setName("One");
        club1 = new Club();
        club1.setId(1L);
        Club club2 = new Club();
        club2.setId(2L);
        match1 = new Match();
        match1.setId(1L);
        match1.setLocation("location1");
        match1.setHomeClub(club1);
        match1.setAwayClub(club2);
        match1.setDate(localDateTime);
        match1.setLeague(league);
        match1.setHomeClubGoalScorers(new ArrayList<>());
        match1.setAwayClubGoalScorers(new ArrayList<>());
        match2 = new Match();
        match2.setId(2L);
        match2.setLocation("location2");
        match2.setHomeClub(club2);
        match2.setAwayClub(club1);
        match2.setDate(localDateTime);
        match2.setLeague(league);
        List<Match> matchList = new ArrayList<>(Arrays.asList(match1, match2));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > matchList.size() ? matchList.size() : (start + pageable.getPageSize());
        matchPage = new PageImpl<>(matchList.subList((int) start, (int) end), pageable, matchList.size());
        matchDTO = new MatchDTO();
        matchDTO.setLeague(league);
        matchDTO.setHomeClub(club1);
        matchDTO.setAwayClub(club2);
        matchResultDTO = new MatchResultDTO();
        matchResultDTO.setHomeClubGoals(2);
        matchResultDTO.setAwayClubGoals(1);
        player1 = new Player();
        player1.setId(1L);
        Player player2 = new Player();
        player2.setId(2L);
        GoalScorer goalScorer1 = new GoalScorer();
        goalScorer1.setPlayer(player1);
        goalScorer1.setNumberOfGoals(2);
        GoalScorer goalScorer2 = new GoalScorer();
        goalScorer2.setPlayer(player2);
        goalScorer2.setNumberOfGoals(1);
        matchResultDTO.setHomeClubGoalScorers(Collections.singletonList(goalScorer1));
        matchResultDTO.setAwayClubGoalScorers(Collections.singletonList(goalScorer2));
    }

    @Test
    public void whenGetMatchesByPhraseInLocation_thenReturnMatchToReturnDTOPage() {
        when(matchRepository.findByLocationContainingIgnoreCase(any(Pageable.class), any(String.class)))
                .thenReturn(Optional.of(matchPage));
        Page<MatchToReturnDTO> foundMatchToReturnDTOPage = matchService.getMatchesByPhraseInLocation(pageable, "o");
        assertThat(foundMatchToReturnDTOPage.getTotalElements())
                .isEqualTo(matchPage.getTotalElements());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getId())
                .isEqualTo(matchPage.getContent().get(0).getId());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getLocation())
                .isEqualTo(matchPage.getContent().get(0).getLocation());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getDate())
                .isEqualTo(matchPage.getContent().get(0).getDate());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getLeague())
                .isEqualTo(matchPage.getContent().get(0).getLeague());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getHomeClub())
                .isEqualTo(matchPage.getContent().get(0).getHomeClub());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getAwayClub())
                .isEqualTo(matchPage.getContent().get(0).getAwayClub());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getHomeClubGoals())
                .isEqualTo(matchPage.getContent().get(0).getHomeClubGoals());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getAwayClubGoals())
                .isEqualTo(matchPage.getContent().get(0).getAwayClubGoals());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getHomeClubGoalScorers())
                .isEqualTo(matchPage.getContent().get(0).getHomeClubGoalScorers());
        assertThat(foundMatchToReturnDTOPage.getContent().get(0).getAwayClubGoalScorers())
                .isEqualTo(matchPage.getContent().get(0).getAwayClubGoalScorers());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getId())
                .isEqualTo(matchPage.getContent().get(1).getId());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getLocation())
                .isEqualTo(matchPage.getContent().get(1).getLocation());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getDate())
                .isEqualTo(matchPage.getContent().get(1).getDate());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getLeague())
                .isEqualTo(matchPage.getContent().get(1).getLeague());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getHomeClub())
                .isEqualTo(matchPage.getContent().get(1).getHomeClub());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getAwayClub())
                .isEqualTo(matchPage.getContent().get(1).getAwayClub());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getHomeClubGoals())
                .isEqualTo(matchPage.getContent().get(1).getHomeClubGoals());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getAwayClubGoals())
                .isEqualTo(matchPage.getContent().get(1).getAwayClubGoals());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getHomeClubGoalScorers())
                .isEqualTo(matchPage.getContent().get(1).getHomeClubGoalScorers());
        assertThat(foundMatchToReturnDTOPage.getContent().get(1).getAwayClubGoalScorers())
                .isEqualTo(matchPage.getContent().get(1).getAwayClubGoalScorers());
    }

    @Test
    public void whenGetMatchesByPhraseInLocation_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> matchService.getMatchesByPhraseInLocation(pageable, "o"));
        assertEquals(exception.getMessage(), "No matches were found associating with this phrase");
    }

    @Test
    public void whenGetMatchById_thenReturnMatchToReturnDTO() {
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match1));
        MatchToReturnDTO foundMatchToReturnDTO = matchService.getMatchById(1L);
        assertThat(foundMatchToReturnDTO.getId()).isEqualTo(match1.getId());
        assertThat(foundMatchToReturnDTO.getLocation()).isEqualTo(match1.getLocation());
        assertThat(foundMatchToReturnDTO.getDate()).isEqualTo(match1.getDate());
        assertThat(foundMatchToReturnDTO.getLeague()).isEqualTo(match1.getLeague());
        assertThat(foundMatchToReturnDTO.getHomeClub()).isEqualTo(match1.getHomeClub());
        assertThat(foundMatchToReturnDTO.getAwayClub()).isEqualTo(match1.getAwayClub());
        assertThat(foundMatchToReturnDTO.getHomeClubGoals()).isEqualTo(match1.getHomeClubGoals());
        assertThat(foundMatchToReturnDTO.getAwayClubGoals()).isEqualTo(match1.getAwayClubGoals());
        assertThat(foundMatchToReturnDTO.getHomeClubGoalScorers()).isEqualTo(match1.getHomeClubGoalScorers());
        assertThat(foundMatchToReturnDTO.getAwayClubGoalScorers()).isEqualTo(match1.getAwayClubGoalScorers());
    }

    @Test
    public void whenGetMatchById_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> matchService.getMatchById(1L));
        assertEquals(exception.getMessage(), "Match with given id does not exist");
    }

    @Test
    public void whenAddMatch_thenSaveMatchInRepository() {
        when(clubRepository.findById(anyLong())).thenReturn(Optional.of(club1));
        when(leagueService.getLeagueById(anyLong())).thenReturn(league);
        when(matchRepository.save(any(Match.class))).thenAnswer(i -> i.getArguments()[0]);
        doNothing().when(leagueService).updateMatchSchedule(any(Match.class));
        matchService.addMatch(matchDTO);
        assertEquals(club1, matchDTO.getHomeClub());
        assertEquals(club1, matchDTO.getAwayClub());
        assertEquals(league, matchDTO.getLeague());
    }

    @Test
    public void whenUpdateMatch_thenSaveMatchInRepository() {
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match2));
        when(matchRepository.save(any(Match.class))).thenAnswer(i -> i.getArguments()[0]);
        matchService.updateMatch(1L, matchDTO);
        assertEquals(matchDTO.getLeague(), match2.getLeague());
        assertEquals(matchDTO.getHomeClub(), match2.getHomeClub());
        assertEquals(matchDTO.getAwayClub(), match2.getAwayClub());
    }

    @Test
    public void whenUpdateMatch_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> matchService.updateMatch(1L, matchDTO));
        assertEquals(exception.getMessage(), "Match with given id does not exist");
    }

    @Test
    public void whenSetGoalsScorers_thenSaveMatchInRepository() {
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match1));
        doNothing().when(clubStatsService).updateClubStatsAfterMatch(any(Match.class));
        doNothing().when(playerStatsService).clearPlayerStatsBeforeMatchUpdate(any(Match.class));
        when(matchRepository.save(any(Match.class))).thenAnswer(i -> i.getArguments()[0]);
        doNothing().when(goalScorerRepository).delete(any(GoalScorer.class));
        when(playerRepository.findById(anyLong())).thenReturn(Optional.of(player1));
        when(goalScorerRepository.save(any(GoalScorer.class))).thenAnswer(i -> i.getArguments()[0]);
        matchService.setGoalScorers(1L, matchResultDTO);
        assertEquals(matchResultDTO.getHomeClubGoals(), match1.getHomeClubGoals());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getPlayer(),
                match1.getHomeClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getHomeClubGoalScorers().get(0).getNumberOfGoals());
        assertEquals(matchResultDTO.getAwayClubGoals(), match1.getAwayClubGoals());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getPlayer(),
                match1.getAwayClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getAwayClubGoalScorers().get(0).getNumberOfGoals());
        assertTrue(match1.isResultAddedToClubStats());
        assertTrue(match1.isGoalScorersAddedToPlayerStats());

        Player player3 = new Player();
        player3.setId(3L);
        GoalScorer goalScorer3 = new GoalScorer();
        goalScorer3.setPlayer(player3);
        goalScorer3.setNumberOfGoals(1);
        match1.getHomeClubGoalScorers().add(goalScorer3);
        matchService.setGoalScorers(1L, matchResultDTO);
        assertEquals(matchResultDTO.getHomeClubGoals(), match1.getHomeClubGoals());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getPlayer(),
                match1.getHomeClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getHomeClubGoalScorers().get(0).getNumberOfGoals());
        assertEquals(matchResultDTO.getAwayClubGoals(), match1.getAwayClubGoals());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getPlayer(),
                match1.getAwayClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getAwayClubGoalScorers().get(0).getNumberOfGoals());
        assertTrue(match1.isResultAddedToClubStats());
        assertTrue(match1.isGoalScorersAddedToPlayerStats());

        matchResultDTO.setAwayClubGoalScorers(Collections.singletonList(goalScorer3));
        matchService.setGoalScorers(1L, matchResultDTO);
        assertEquals(matchResultDTO.getHomeClubGoals(), match1.getHomeClubGoals());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getPlayer(),
                match1.getHomeClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getHomeClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getHomeClubGoalScorers().get(0).getNumberOfGoals());
        assertEquals(matchResultDTO.getAwayClubGoals(), match1.getAwayClubGoals());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getPlayer(),
                match1.getAwayClubGoalScorers().get(0).getPlayer());
        assertEquals(matchResultDTO.getAwayClubGoalScorers().get(0).getNumberOfGoals(),
                match1.getAwayClubGoalScorers().get(0).getNumberOfGoals());
        assertTrue(match1.isResultAddedToClubStats());
        assertTrue(match1.isGoalScorersAddedToPlayerStats());
    }

    @Test
    public void whenSetGoalsScorers_thenThrowDateTimeException() {
        Match match = new Match();
        match.setDate(LocalDateTime.of(2222, 12, 12, 12, 12, 12));
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match));
        Throwable exception = assertThrows(DateTimeException.class,
                () -> matchService.setGoalScorers(1L, matchResultDTO));
        assertEquals(exception.getMessage(), "You can not set the result for a future match");
    }

    @Test
    public void whenSetNumberOfGoals_thenSaveMatchInRepository() {
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match1));
        doNothing().when(clubStatsService).updateClubStatsAfterMatch(any(Match.class));
        when(matchRepository.save(any(Match.class))).thenAnswer(i -> i.getArguments()[0]);
        matchService.setNumberOfGoals(1L, matchResultDTO);
        assertEquals(matchResultDTO.getHomeClubGoals(), match1.getHomeClubGoals());
        assertEquals(matchResultDTO.getAwayClubGoals(), match1.getAwayClubGoals());
        assertTrue(match1.isResultAddedToClubStats());

        doNothing().when(clubStatsService).clearClubStatsBeforeMatchUpdate(any(Match.class));
        matchService.setNumberOfGoals(1L, matchResultDTO);
        assertEquals(matchResultDTO.getHomeClubGoals(), match1.getHomeClubGoals());
        assertEquals(matchResultDTO.getAwayClubGoals(), match1.getAwayClubGoals());
        assertTrue(match1.isResultAddedToClubStats());
    }

    @Test
    public void whenSetNumberOfGoals_thenThrowDateTimeException() {
        Match match = new Match();
        match.setDate(LocalDateTime.of(2222, 12, 12, 12, 12, 12));
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match));
        Throwable exception = assertThrows(DateTimeException.class,
                () -> matchService.setNumberOfGoals(1L, matchResultDTO));
        assertEquals(exception.getMessage(), "You can not set the result for a future match");
    }

    @Test
    public void whenSetNumberOfGoals_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> matchService.setNumberOfGoals(1L, matchResultDTO));
        assertEquals(exception.getMessage(), "Match with given id does not exist");
    }

    @Test
    public void whenDeleteMatchById_thenDeleteMatchFromRepository() {
        match2.setResultAddedToClubStats(true);
        match2.setGoalScorersAddedToPlayerStats(true);
        when(matchRepository.findById(any(Long.class))).thenReturn(Optional.of(match2));
        doNothing().when(clubStatsService).clearClubStatsBeforeMatchUpdate(any(Match.class));
        doNothing().when(playerStatsService).clearPlayerStatsBeforeMatchUpdate(any(Match.class));
        doNothing().when(matchRepository).delete(any(Match.class));
        matchService.deleteMatchById(1L);
    }

}
