package com.example.project.serviceImpl;

import com.example.project.model.Club;
import com.example.project.model.Player;
import com.example.project.repository.ClubRepository;
import com.example.project.repository.PlayerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {
    private final Pageable pageable = PageRequest.of(0, 10);
    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private ClubRepository clubRepository;
    @InjectMocks
    private PlayerServiceImpl playerService;

    private Player playerWithoutClub;
    private Player playerWithClub;
    private Page<Player> playerPage;
    private Club club;

    @Before
    public void init() {
        playerWithoutClub = new Player();
        playerWithoutClub.setName("One");
        playerWithClub = new Player();
        playerWithClub.setName("Two");
        List<Player> playerList = new ArrayList<>(Arrays.asList(playerWithoutClub, playerWithClub));
        long start = pageable.getOffset();
        long end = (start + pageable.getPageSize()) > playerList.size() ? playerList.size() : (start + pageable.getPageSize());
        playerPage = new PageImpl<>(playerList.subList((int) start, (int) end), pageable, playerList.size());
        club = new Club();
        club.setId(1L);
        playerWithClub.setClub(club);
    }

    @Test
    public void whenGetPlayersByPhraseInName_thenReturnPlayerPage() {
        when(playerRepository.findByNameContainingIgnoreCase(any(Pageable.class), any(String.class)))
                .thenReturn(Optional.of(playerPage));
        Page<Player> returnedPlayerPage = playerService.getPlayersByPhraseInName(pageable, "o");
        assertThat(returnedPlayerPage.getTotalElements()).isEqualTo(returnedPlayerPage.getTotalElements());
        assertEquals(playerWithoutClub, returnedPlayerPage.getContent().get(0));
        assertEquals(playerWithClub, returnedPlayerPage.getContent().get(1));
    }

    @Test
    public void whenGetPlayersByPhraseInName_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> playerService.getPlayersByPhraseInName(pageable, "o"));
        assertEquals(exception.getMessage(), "No players were found associating with this phrase");
    }

    @Test
    public void whenGetPlayerById_thenReturnPlayer() {
        when(playerRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(playerWithoutClub));
        Player foundPlayer = playerService.getPlayerById(1L);
        assertThat(foundPlayer.getId()).isEqualTo(playerWithoutClub.getId());
        assertThat(foundPlayer.getName()).isEqualTo(playerWithoutClub.getName());
        assertThat(foundPlayer.getClub()).isEqualTo(playerWithoutClub.getClub());
    }

    @Test
    public void whenGetPlayerById_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> playerService.getPlayerById(1L));
        assertEquals(exception.getMessage(), "Player with given id does not exist");
    }

    @Test
    public void whenAddPlayer_thenSavePlayerInRepository() {
        playerService.addPlayer(playerWithoutClub);

        when(clubRepository.findById(any(Long.class))).thenReturn(Optional.of(club));
        playerService.addPlayer(playerWithClub);
    }

    @Test
    public void whenUpdatePlayer_thenSavePlayerInRepository() {
        when(playerRepository.save(any(Player.class))).thenAnswer(i -> i.getArguments()[0]);
        when(playerRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(playerPage.getContent().get(0)));
        Player updatedPlayerWithoutClub = playerPage.getContent().get(0);
        playerService.updatePlayer(1L, updatedPlayerWithoutClub);

        when(clubRepository.findById(any(Long.class))).thenReturn(Optional.of(club));
        Player updatedPlayerWithCLub = playerPage.getContent().get(1);
        playerService.updatePlayer(1L, updatedPlayerWithCLub);
    }

    @Test
    public void whenUpdatePlayer_thenThrowEntityNotFoundException() {
        Throwable exception = assertThrows(EntityNotFoundException.class,
                () -> playerService.updatePlayer(1L, playerPage.getContent().get(0)));
        assertEquals(exception.getMessage(), "Player with given id does not exist");
    }

    @Test
    public void whenDeletePlayerById_thenDeletePlayerFromRepository() {
        when(playerRepository.findById(any(Long.class)))
                .thenReturn(Optional.of(playerPage.getContent().get(0)));
        doNothing().when(playerRepository).delete(any(Player.class));
        playerService.deletePlayerById(1L);
    }
}
