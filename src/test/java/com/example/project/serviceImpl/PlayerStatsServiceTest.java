package com.example.project.serviceImpl;

import com.example.project.dto.PlayerStatsDTO;
import com.example.project.model.*;
import com.example.project.repository.PlayerStatsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerStatsServiceTest {
    @Mock
    private PlayerStatsRepository playerStatsRepository;

    @InjectMocks
    private PlayerStatsServiceImpl playerStatsService;

    private Match match;
    private PlayerStats playerStats1;
    @Mock
    private PlayerStatsDTO playerStatsDTO;

    @Before
    public void init() {
        League league = new League();
        league.setId(1L);
        match = new Match();
        match.setLeague(league);
        Player player1 = new Player();
        player1.setId(1L);
        Player player2 = new Player();
        player2.setId(2L);
        GoalScorer goalScorer1 = new GoalScorer();
        goalScorer1.setPlayer(player1);
        goalScorer1.setNumberOfGoals(1);
        GoalScorer goalScorer2 = new GoalScorer();
        goalScorer2.setPlayer(player2);
        goalScorer2.setNumberOfGoals(2);
        match.setHomeClubGoalScorers(Collections.singletonList(goalScorer1));
        match.setHomeClubGoals(1);
        match.setAwayClubGoalScorers(Collections.singletonList(goalScorer2));
        match.setAwayClubGoals(2);
        playerStats1 = new PlayerStats();
        playerStats1.setId(1L);
        playerStats1.setPlayer(player1);
        playerStats1.setNumberOfHomeGoals(1);
        playerStats1.setNumberOfAwayGoals(2);
    }


    @Test
    public void whenGetSetOfPlayerStatsByPlayerId_thenReturnSetOfPlayerStats() {
        when(playerStatsRepository.findSetOfPlayerStatsByPlayerId(any(Long.class)))
                .thenReturn(new HashSet<>(Collections.singletonList(playerStatsDTO)));
        Set<PlayerStatsDTO> playerStatsDTOSet = playerStatsService.getSetOfPlayerStatsByPlayerId(1L);
        assertThat(playerStatsDTOSet.iterator().next()).isEqualTo(playerStatsDTO);
    }

    @Test
    public void whenUpdatePlayerStatsAfterMatch_thenSavePlayerStatsInRepository() {
        when(playerStatsRepository.findByLeagueIdAndPlayerId(any(Long.class), any(Long.class))).thenReturn(null);
        when(playerStatsRepository.save(any(PlayerStats.class))).thenReturn(playerStats1);
        playerStatsService.updatePlayerStatsAfterMatch(match);
        assertEquals(match.getHomeClubGoals(), playerStats1.getNumberOfHomeGoals());
        assertEquals(match.getAwayClubGoals(), playerStats1.getNumberOfAwayGoals());
        assertEquals(match.getHomeClubGoals() + match.getAwayClubGoals(), playerStats1.getSumOfGoals());
    }

    @Test
    public void whenClearPlayerStatsBeforeMatchUpdate_thenSavePlayerStatsInRepository() {
        when(playerStatsRepository.findByLeagueIdAndPlayerId(any(Long.class), any(Long.class)))
                .thenReturn(playerStats1);
        when(playerStatsRepository.save(any(PlayerStats.class))).thenReturn(playerStats1);
        playerStatsService.clearPlayerStatsBeforeMatchUpdate(match);
        assertEquals(0, playerStats1.getNumberOfHomeGoals());
        assertEquals(0, playerStats1.getNumberOfAwayGoals());
        assertEquals(0, playerStats1.getSumOfGoals());
    }

    @Test
    public void whenDeletePlayerStats_thenDeletePlayerStatsFromRepository() {
        when(playerStatsRepository.findById(any(Long.class))).thenReturn(Optional.of(playerStats1));
        doNothing().when(playerStatsRepository).delete(any(PlayerStats.class));
        playerStatsService.deletePlayerStats(1L);
    }
}
